package com.daslernen.jsonschemaeditor.validation;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.*;
import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.ERROR;
import static java.lang.String.format;
import static java.util.Collections.singleton;

final class ValidationVisitor<T> {

    private static final Logger LOG = LoggerFactory.getLogger(ValidationVisitor.class);

    private final Result<T> result;

    public static <T> ValidationVisitor<T> create(final Supplier<Result<T>> action) {
        return createSafe(Collections.emptySet(), action);
    }

    private ValidationVisitor(final Result<T> result) {
        Preconditions.checkNotNull(result);
        this.result = result;
    }

    public <U> ValidationVisitor<U> map(final Function<T, Result<U>> action) {
        if (alreadyContainsErrors()) {
            return new ValidationVisitor<>(Result.result(result.messages));
        }
        return createSafe(getMessages(), () -> action.apply(result.value));
    }

    public <U, V> ValidationVisitor<V> merge(final ValidationVisitor<U> visitor, final BiFunction<T, U, V> mapper) {
        final Set<ValidationMessage> mergedMessages = Sets.union(getMessages(), visitor.getMessages());
        final Optional<ValidationMessage> error = getFirstError(mergedMessages);
        if (error.isPresent()) {
            return new ValidationVisitor<>(Result.result(mergedMessages));
        }

        return new ValidationVisitor<>(calculateResult(visitor, mergedMessages, mapper));
    }

    private boolean alreadyContainsErrors() {
        return result.messages.stream().anyMatch(message -> message.getStatus() == ERROR);
    }

    private <V, U> Result<V> calculateResult(final ValidationVisitor<U> visitor, final Set<ValidationMessage> mergedMessages,
                                             final BiFunction<T, U, V> mapper) {

        final T thisValue = this.result.value;
        final U visitorValue = visitor.result.value;
        try {
            final V mergedValue = mapper.apply(thisValue, visitorValue);
            return Result.result(mergedMessages, mergedValue);
        } catch (Exception e) {
            return Result.errorResult(format("Failed to combine [%s] with [%s]", thisValue, visitorValue));
        }
    }

    private Optional<ValidationMessage> getFirstError(final Set<ValidationMessage> messages) {
        return messages.stream()
                .filter(message -> message.getStatus() == ERROR)
                .findFirst();
    }

    public Result<T> getResult() {
        return result;
    }

    public SortedSet<ValidationMessage> getMessages() {
        return result.getMessages();
    }

    private static <T> ValidationVisitor<T> createSafe(final Set<ValidationMessage> messages, final Supplier<Result<T>> action) {
        final Result<T> newResult;
        try {
            newResult = action.get();
        } catch (Exception e) {
            final String errorMessage = format("Internal error caused by [%s]", e);
            LOG.error(errorMessage, e);
            final Set<ValidationMessage> mergedMessages = Sets.union(messages, Collections.singleton(error(errorMessage)));
            return new ValidationVisitor<>(Result.result(mergedMessages));
        }
        final Set<ValidationMessage> mergedMessages = Sets.union(messages, newResult.getMessages());
        return new ValidationVisitor<>(Result.result(mergedMessages, newResult.getValue().orElse(null)));
    }

    static final class Result<T> {

        private final SortedSet<ValidationMessage> messages;
        private final T value;

        public static <T> Result<T> okResult(final T value) {
            return result(singleton(ok()), value);
        }

        static <T> Result<T> result(final Set<ValidationMessage> messages) {
            return result(messages, null);
        }

        public static <T> Result<T> result(final Set<ValidationMessage> messages, final T value) {
            return new Result<>(messages, value);
        }

        public static <T> Result<T> errorResult(final String messageText) {
            return result(singleton(error(messageText)), null);
        }

        private Result(final Set<ValidationMessage> messages, final T value) {
            Preconditions.checkNotNull(messages);
            final Set<ValidationMessage> nonOkMessages = messages.stream()
                    .filter(message -> message.getStatus() != ValidationStatus.OK)
                    .collect(Collectors.toSet());
            this.messages = ImmutableSortedSet.copyOf(SEVERITY_COMPARATOR, nonOkMessages);
            this.value = value;
        }

        SortedSet<ValidationMessage> getMessages() {
            return messages;
        }

        public Optional<T> getValue() {
            return Optional.ofNullable(value);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("messages", messages)
                    .add("valueType", value != null ? value.getClass().getSimpleName() : null)
                    .add("value", value)
                    .toString();
        }
    }

    static final class None {
    }
}
