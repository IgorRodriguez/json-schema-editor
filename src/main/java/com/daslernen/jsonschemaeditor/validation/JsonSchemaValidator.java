package com.daslernen.jsonschemaeditor.validation;

import java.util.SortedSet;

public interface JsonSchemaValidator {

    /**
     * Validate a JSON document against a JSON schema.
     * @param jsonDocument The json to validate.
     * @param schemaDocument The json schema to validate the json document against to
     * @return A description of the validation
     */
    SortedSet<ValidationMessage> validateAgainstSchema(RawDocument jsonDocument, RawDocument schemaDocument);
}
