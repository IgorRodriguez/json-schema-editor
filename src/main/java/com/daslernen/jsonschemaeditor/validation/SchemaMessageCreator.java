package com.daslernen.jsonschemaeditor.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.google.common.collect.ImmutableSortedSet;

import java.util.EnumSet;
import java.util.SortedSet;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.*;
import static java.lang.String.format;

final class SchemaMessageCreator {

    private static final EnumSet<LogLevel> ERROR_LEVELS = EnumSet.of(LogLevel.WARNING, LogLevel.ERROR, LogLevel.FATAL);

    /**
     * Converts a report in a set of messages sorted by severity.
     * @param report The report to transform.
     * @return A set of messages sorted by severity, highest first.
     */
    public SortedSet<ValidationMessage> toMessages(final ProcessingReport report) {
        return ImmutableSortedSet.copyOf(SEVERITY_COMPARATOR, StreamSupport.stream(report.spliterator(), true)
                .filter(this::byLogLevel)
                .map(this::toValidationMessage)
                .sorted(this::sortByStatus)
                .collect(Collectors.toList()));
    }

    private boolean byLogLevel(final ProcessingMessage processingMessage) {
        return ERROR_LEVELS.contains(processingMessage.getLogLevel());
    }

    private ValidationMessage toValidationMessage(final ProcessingMessage message) {
        final LogLevel logLevel = message.getLogLevel();
        final JsonNode pointer = message.asJson().path("schema").path("pointer");
        if (isError(logLevel)) {
            return error(composeMessage(pointer, message));
        } else if (isWarning(logLevel)) {
            return warn((composeMessage(pointer, message)));
        }
        throw new IllegalArgumentException(format("Message should be have level WARNING at least, but was [%s]", message));
    }

    private String composeMessage(final JsonNode pointer, final ProcessingMessage processingMessage) {
        final String message = processingMessage.getMessage();
        final String pointerText = pointer.asText();
        if (pointerText.isEmpty()) {
            return message;
        }
        return String.format("[%s] %s", pointerText, message);
    }

    private boolean isError(final LogLevel logLevel) {
        return logLevel == LogLevel.FATAL || logLevel == LogLevel.ERROR;
    }

    private boolean isWarning(LogLevel logLevel) {
        return logLevel == LogLevel.WARNING;
    }

    private int sortByStatus(final ValidationMessage m1, final ValidationMessage m2) {
        return ValidationStatus.descendingLogLevelComparator().compare(m1.getStatus(), m2.getStatus());
    }
}
