package com.daslernen.jsonschemaeditor.validation;

import java.util.SortedSet;

public interface Validator {

    SortedSet<ValidationMessage> validate(RawDocument value);
}
