package com.daslernen.jsonschemaeditor.validation;

import com.daslernen.jsonschemaeditor.domain.JsonMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import javax.inject.Inject;
import java.util.SortedSet;

import static com.daslernen.jsonschemaeditor.validation.ValidationVisitor.Result.*;
import static java.lang.String.format;

public final class SchemaValidator extends JsonValidator implements JsonSchemaValidator {

    private final JsonSchemaFactory jsonSchemaFactory;
    private final SchemaMessageCreator schemaMessageCreator;

    @Inject
    public SchemaValidator(final JsonMapper jsonMapper,
                           final JsonSchemaFactory jsonSchemaFactory,
                           final SchemaMessageCreator schemaMessageCreator) {
        super(jsonMapper);
        this.jsonSchemaFactory = jsonSchemaFactory;
        this.schemaMessageCreator = schemaMessageCreator;
    }

    @Override
    public SortedSet<ValidationMessage> validate(final RawDocument schemaDocument) {
        return validateSchema(schemaDocument)
                .getMessages();
    }

    @Override
    public SortedSet<ValidationMessage> validateAgainstSchema(final RawDocument jsonDocument, final RawDocument schemaDocument) {
        return validateSchema(schemaDocument)
                .map(this::toSchema)
                .merge(validateJson(jsonDocument), this::toJsonAndSchema)
                .map(this::checkValid)
                .map(this::toValidationMessage)
                .getMessages();
    }

    private ValidationVisitor<JsonNode> validateSchema(final RawDocument schemaDocument) {
        return validateJson(schemaDocument)
                .map(this::validateCorrectSchemaSyntax);
    }

    private ValidationVisitor.Result<JsonNode> validateCorrectSchemaSyntax(final JsonNode schemaNode) {
        final ProcessingReport report = jsonSchemaFactory.getSyntaxValidator().validateSchema(schemaNode);
        return result(schemaMessageCreator.toMessages(report), schemaNode);
    }

    private ValidationVisitor.Result<JsonSchema> toSchema(final JsonNode schemaNode) {
        try {
            return okResult(jsonSchemaFactory.getJsonSchema(schemaNode));
        } catch (ProcessingException e) {
            return errorResult(format("Schema is not valid due to [%s]", e));
        }
    }

    private JsonAndSchema toJsonAndSchema(final JsonSchema schema, final JsonNode json) {
        return new JsonAndSchema(json, schema);
    }

    private ValidationVisitor.Result<ProcessingReport> checkValid(final JsonAndSchema jsonAndSchema) {
        try {
            final ProcessingReport report = jsonAndSchema.schema.validate(jsonAndSchema.json);
            return okResult(report);
        } catch (ProcessingException e) {
            return errorResult(e.getMessage());
        }
    }

    private ValidationVisitor.Result<ValidationVisitor.None> toValidationMessage(final ProcessingReport report) {
        return result(schemaMessageCreator.toMessages(report), new ValidationVisitor.None());
    }

    private static final class JsonAndSchema {
        private final JsonNode json;
        private final JsonSchema schema;

        JsonAndSchema(final JsonNode json, final JsonSchema schema) {
            this.json = json;
            this.schema = schema;
        }
    }
}
