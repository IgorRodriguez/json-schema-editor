package com.daslernen.jsonschemaeditor.validation;

import com.fasterxml.jackson.core.JsonPointer;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public final class Document<T> {

    private static final JsonPointer ROOT_PATH = JsonPointer.compile("");

    private final T value;
    private final JsonPointer pathToRoot;

    public Document(final T value) {
        this(value, ROOT_PATH);
    }

    Document(final T value, final JsonPointer pathToRoot) {
        Preconditions.checkNotNull(value);
        Preconditions.checkNotNull(pathToRoot);

        this.value = value;
        this.pathToRoot = pathToRoot;
    }

    /**
     * Check whether an string is a valid json path.
     * @param jsonPath The string to test.
     * @return True if the path is valid.
     * @see <a href = https://tools.ietf.org/html/draft-ietf-appsawg-json-pointer-03>JSON pointer specification</a>
     */
    public static boolean isValidPath(final String jsonPath) {
        try {
            JsonPointer.compile(jsonPath);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public T getValue() {
        return value;
    }

    public JsonPointer getPathToRoot() {
        return pathToRoot;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .add("pathToRoot", pathToRoot)
                .toString();
    }
}
