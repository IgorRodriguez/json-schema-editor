package com.daslernen.jsonschemaeditor.validation;

import com.fasterxml.jackson.core.JsonLocation;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;
import java.util.Objects;

import static com.daslernen.jsonschemaeditor.validation.ValidationStatus.*;
import static java.lang.String.format;

public final class ValidationMessage {

    static final Comparator<ValidationMessage> SEVERITY_COMPARATOR = (m1, m2) -> ComparisonChain.start()
            .compare(m1.status, m2.status, ValidationStatus.descendingLogLevelComparator())
            .compare(m1.text, m2.text)
            .result();
    private static final String OK_MESSAGE_TEXT = "Valid";
    private static final ValidationMessage OK_MESSAGE = new ValidationMessage(OK, OK_MESSAGE_TEXT);
    private static final int MAX_MESSAGE_LENGTH = 100;

    private final ValidationStatus status;
    private final String text;
    private final String fullText;

    public static ValidationMessage ok() {
        return OK_MESSAGE;
    }

    public static ValidationMessage warn(final String message) {
        return new ValidationMessage(WARNING, message);
    }

    public static ValidationMessage error(final String message) {
        return new ValidationMessage(ERROR, message);
    }

    public static ValidationMessage jsonError(final JsonLocation location, final String message) {
        Preconditions.checkNotNull(message);
        return error(format("(%d:%d) %s", location.getLineNr(), location.getColumnNr(), message));
    }

    ValidationMessage(final ValidationStatus status, final String text) {
        Preconditions.checkNotNull(status);
        Preconditions.checkNotNull(text);

        this.status = status;
        this.text = shortenText(text);
        this.fullText = text;
    }

    public String getText() {
        return text;
    }

    public ValidationStatus getStatus() {
        return status;
    }

    public String getFullText() {
        return fullText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationMessage that = (ValidationMessage) o;
        return status == that.status &&
                Objects.equals(text, that.text) &&
                Objects.equals(fullText, that.fullText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, text, fullText);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("status", status)
                .add("text", text)
                .add("fullText", fullText)
                .toString();
    }

    private String shortenText(final String text) {
        if (text.length() > MAX_MESSAGE_LENGTH) {
            return StringUtils.substring(text, 0, MAX_MESSAGE_LENGTH - 3).trim() + "...";
        }
        return text;
    }
}
