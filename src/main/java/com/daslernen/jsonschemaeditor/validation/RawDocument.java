package com.daslernen.jsonschemaeditor.validation;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

public final class RawDocument {

    private static final String ROOT_PATH = "";

    private final String value;
    private final String pathToRoot;

    public RawDocument(final String value) {
        this(value, ROOT_PATH);
    }

    public RawDocument(final String value, final String pathToRoot) {
        Preconditions.checkNotNull(value);
        Preconditions.checkNotNull(pathToRoot);

        this.value = value;
        this.pathToRoot = pathToRoot;
    }

    public String getValue() {
        return value;
    }

    String getPathToRoot() {
        return pathToRoot;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .add("pathToRoot", pathToRoot)
                .toString();
    }
}
