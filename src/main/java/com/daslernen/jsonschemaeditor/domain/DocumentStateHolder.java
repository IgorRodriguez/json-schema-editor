package com.daslernen.jsonschemaeditor.domain;

import com.google.common.base.Preconditions;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

final class DocumentStateHolder {

    private final List<Consumer<CurrentFile>> listeners = new ArrayList<>();

    private CurrentFile state = CurrentFile.NONE;

    void addStateChangeListener(final Consumer<CurrentFile> listener) {
        listeners.add(listener);
    }

    CurrentFile getState() {
        return state;
    }

    void updateState(final Path path, final String newContent) {
        Preconditions.checkNotNull(path);

        updateAndNotifyListeners(new CurrentFile(path, newContent));
    }

    void updateState(final Path path, final String newContent, final NewLineStyle newLineStyle) {
        Preconditions.checkNotNull(path);

        updateAndNotifyListeners(new CurrentFile(path, newContent, newLineStyle));
    }

    void updateToNone() {
        updateAndNotifyListeners(CurrentFile.NONE);
    }

    private void updateAndNotifyListeners(final CurrentFile newState) {
        if (!state.equals(newState)) {
            state = newState;
            listeners.forEach(listeners -> listeners.accept(newState));
        }
    }
}
