package com.daslernen.jsonschemaeditor.domain;

import com.google.inject.Provider;
import javafx.util.StringConverter;

import javax.inject.Inject;
import java.text.NumberFormat;

public final class PercentageStringConverter extends StringConverter<Double> {

    private final Provider<NumberFormat> formatterProvider;

    @Inject
    public PercentageStringConverter(final Provider<NumberFormat> formatterProvider) {
        this.formatterProvider = formatterProvider;
    }

    @Override
    public String toString(final Double value) {
        final NumberFormat formatter = formatterProvider.get();
        formatter.setMaximumFractionDigits(0);
        formatter.setMinimumFractionDigits(0);
        formatter.setParseIntegerOnly(true);
        return formatter.format(value / 100);
    }

    @Override
    public Double fromString(final String stringValue) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
