package com.daslernen.jsonschemaeditor.service;

import com.google.inject.AbstractModule;

public final class ServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(FileService.class).to(DefaultFileService.class);
    }
}
