package com.daslernen.jsonschemaeditor;

import com.daslernen.jsonschemaeditor.configuration.Configuration;
import com.daslernen.jsonschemaeditor.configuration.ConfigurationLoader;
import com.daslernen.jsonschemaeditor.configuration.ConfigurationModule;
import com.daslernen.jsonschemaeditor.domain.DomainModule;
import com.daslernen.jsonschemaeditor.service.ServiceModule;
import com.daslernen.jsonschemaeditor.ui.StageInitialiser;
import com.daslernen.jsonschemaeditor.ui.UiModule;
import com.daslernen.jsonschemaeditor.update.ApplicationUpdater;
import com.daslernen.jsonschemaeditor.update.UpdateModule;
import com.daslernen.jsonschemaeditor.validation.ValidationModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class JsonSchemaEditor extends Application {

    private static final Logger LOG = LoggerFactory.getLogger(JsonSchemaEditor.class);

    private Injector injector;

    @Override
    public void start(final Stage stage) {
        LOG.info("Starting the application");
        injector = Guice.createInjector(new ConfigurationModule(),
                new UpdateModule(),
                new ValidationModule(),
                new ServiceModule(),
                new DomainModule(),
                new UiModule(stage));
        final StageInitialiser stageInitialiser = injector.getInstance(StageInitialiser.class);
        stageInitialiser.init(stage);
        injector.getInstance(ApplicationUpdater.class).update();
    }

    @Override
    public void stop() {
        LOG.info("Stopping the application");
        final ConfigurationLoader configurationLoader = injector.getInstance(ConfigurationLoader.class);
        configurationLoader.save(injector.getInstance(Configuration.class));
    }
}
