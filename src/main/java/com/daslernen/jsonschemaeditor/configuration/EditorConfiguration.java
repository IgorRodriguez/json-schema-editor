package com.daslernen.jsonschemaeditor.configuration;

import com.google.common.base.MoreObjects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class EditorConfiguration {

    private String title;
    private Path lastOpenDirectory = Paths.get("/");
    private OpenFile openFile = new OpenFile();
    private final StringProperty pathToRoot = new SimpleStringProperty("");

    public String getTitle() {
        return title;
    }

    public EditorConfiguration setTitle(String title) {
        this.title = title;
        return this;
    }

    public Path getLastOpenDirectory() {
        return lastOpenDirectory;
    }

    public EditorConfiguration setLastOpenDirectory(Path lastOpenDirectory) {
        this.lastOpenDirectory = lastOpenDirectory;
        return this;
    }

    public OpenFile getOpenFile() {
        return openFile;
    }

    public EditorConfiguration setOpenFile(OpenFile openFile) {
        this.openFile = openFile;
        return this;
    }

    public String getPathToRoot() {
        return pathToRoot.getValue();
    }

    public EditorConfiguration setPathToRoot(String pathToRoot) {
        this.pathToRoot.setValue(pathToRoot);
        return this;
    }

    public StringProperty pathToRootProperty() {
        return pathToRoot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EditorConfiguration that = (EditorConfiguration) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(lastOpenDirectory, that.lastOpenDirectory) &&
                Objects.equals(openFile, that.openFile) &&
                Objects.equals(pathToRoot.getValue(), that.pathToRoot.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, lastOpenDirectory, openFile, pathToRoot.getValue());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("title", title)
                .add("lastOpenDirectory", lastOpenDirectory)
                .add("openFile", openFile)
                .add("pathToRoot", pathToRoot)
                .toString();
    }
}
