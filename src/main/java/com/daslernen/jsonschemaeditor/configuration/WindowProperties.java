package com.daslernen.jsonschemaeditor.configuration;

import com.google.common.base.MoreObjects;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.util.Objects;

public class WindowProperties {

    private static final double DEFAULT_SCALE = 100;

    private final DoubleProperty x = new SimpleDoubleProperty();
    private final DoubleProperty y = new SimpleDoubleProperty();
    private final DoubleProperty width = new SimpleDoubleProperty();
    private final DoubleProperty height = new SimpleDoubleProperty();
    private final DoubleProperty scale = new SimpleDoubleProperty(DEFAULT_SCALE);

    public double getX() {
        return x.get();
    }

    public WindowProperties setX(double x) {
        this.x.set(x);
        return this;
    }

    public DoubleProperty xProperty() {
        return x;
    }

    public double getY() {
        return y.get();
    }

    public DoubleProperty yProperty() {
        return y;
    }

    public WindowProperties setY(double y) {
        this.y.set(y);
        return this;
    }

    public double getWidth() {
        return width.get();
    }

    public DoubleProperty widthProperty() {
        return width;
    }

    public WindowProperties setWidth(double width) {
        this.width.set(width);
        return this;
    }

    public double getHeight() {
        return height.get();
    }

    public DoubleProperty heightProperty() {
        return height;
    }

    public WindowProperties setHeight(double height) {
        this.height.set(height);
        return this;
    }

    public double getScale() {
        return scale.get();
    }

    public DoubleProperty scaleProperty() {
        return scale;
    }

    public WindowProperties setScale(double scale) {
        this.scale.set(scale);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WindowProperties that = (WindowProperties) o;
        return Objects.equals(x.getValue(), that.x.getValue()) &&
                Objects.equals(y.getValue(), that.y.getValue()) &&
                Objects.equals(width.getValue(), that.width.getValue()) &&
                Objects.equals(height.getValue(), that.height.getValue()) &&
                Objects.equals(scale.getValue(), that.scale.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(x.getValue(), y.getValue(), width.getValue(), height.getValue(), scale.getValue());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("x", x.getValue())
                .add("y", y.getValue())
                .add("width", width.getValue())
                .add("height", height.getValue())
                .add("scale", scale.getValue())
                .toString();
    }
}
