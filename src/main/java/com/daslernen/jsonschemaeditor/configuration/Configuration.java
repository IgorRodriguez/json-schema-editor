package com.daslernen.jsonschemaeditor.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.util.Objects;

public final class Configuration {

    @JsonProperty
    private WindowProperties windowProperties = new WindowProperties();

    @JsonProperty
    private EditorConfiguration jsonEditor = new EditorConfiguration();

    @JsonProperty
    private EditorConfiguration schemaEditor = new EditorConfiguration();

    public EditorConfiguration getJsonEditor() {
        return jsonEditor;
    }

    public Configuration setJsonEditor(EditorConfiguration jsonEditor) {
        this.jsonEditor = jsonEditor;
        return this;
    }

    public EditorConfiguration getSchemaEditor() {
        return schemaEditor;
    }

    public Configuration setSchemaEditor(EditorConfiguration schemaEditor) {
        this.schemaEditor = schemaEditor;
        return this;
    }

    public WindowProperties getWindowProperties() {
        return windowProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuration that = (Configuration) o;
        return Objects.equals(windowProperties, that.windowProperties) &&
                Objects.equals(jsonEditor, that.jsonEditor) &&
                Objects.equals(schemaEditor, that.schemaEditor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(windowProperties, jsonEditor, schemaEditor);
    }

    public Configuration setWindowProperties(WindowProperties windowProperties) {
        this.windowProperties = windowProperties;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("windowProperties", windowProperties)
                .add("jsonEditor", jsonEditor)
                .add("schemaEditor", schemaEditor)
                .toString();
    }
}
