package com.daslernen.jsonschemaeditor.configuration;

import com.daslernen.jsonschemaeditor.domain.JsonMapper;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@Singleton
public final class ConfigurationLoader {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationLoader.class);
    private static final Path EXECUTION_PATH = Paths.get(new File(".").getAbsolutePath()).getParent();
    private static final Path RELATIVE_FILE_PATH = Paths.get("editor.conf");
    private static final Path ABSOLUTE_FILE_PATH = EXECUTION_PATH.resolve(RELATIVE_FILE_PATH);

    private final JsonMapper jsonMapper;

    @Inject
    public ConfigurationLoader(final JsonMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
    }

    Configuration load() {
        return jsonMapper.readFromFile(ABSOLUTE_FILE_PATH, Configuration.class)
                .onFailure(e -> LOG.warn("Couldn't read configuration file on [{}] due to [{}]", ABSOLUTE_FILE_PATH, e.getMessage()))
                .getOrElse(this::createDefaultConfiguration);
    }

    public void save(final Configuration configuration) {
        jsonMapper.writeToFile(ABSOLUTE_FILE_PATH, configuration)
                .onSuccess(v -> LOG.info("Configuration saved to [{}]", ABSOLUTE_FILE_PATH))
                .onFailure(e -> LOG.error("Error saving configuration in [{}], caused by [{}]", ABSOLUTE_FILE_PATH, e.getMessage()));
    }

    private Configuration createDefaultConfiguration() {
        LOG.info("Creating default configuration");
        return new Configuration()
                .setJsonEditor(new EditorConfiguration()
                        .setTitle("JSON"))
                .setSchemaEditor(new EditorConfiguration()
                        .setTitle("Schema"));
    }
}
