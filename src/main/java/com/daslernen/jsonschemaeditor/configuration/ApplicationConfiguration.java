package com.daslernen.jsonschemaeditor.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import java.util.Objects;

public class ApplicationConfiguration {

    private final String updateUrl;

    @JsonCreator
    public ApplicationConfiguration(@JsonProperty("updateUrl") final String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplicationConfiguration that = (ApplicationConfiguration) o;
        return Objects.equals(updateUrl, that.updateUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(updateUrl);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("updateUrl", updateUrl)
                .toString();
    }
}
