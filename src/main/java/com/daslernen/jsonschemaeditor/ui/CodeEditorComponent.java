package com.daslernen.jsonschemaeditor.ui;

import javafx.beans.value.ObservableValue;

import java.util.function.Consumer;

interface CodeEditorComponent extends Component {

    void addTextChangeListener(Consumer<String> listener);

    String getText();

    ObservableValue<Integer> currentParagraphProperty();

    ObservableValue<Integer> caretColumnProperty();

    void replaceText(String s);

    void moveCaretToStartOfDocument();

    int getCaretPosition();

    void selectRange(int start, int end);
}
