package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration;
import com.daslernen.jsonschemaeditor.ui.highlight.JsonHighlighter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Preconditions;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import org.fxmisc.flowless.Virtualized;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.wellbehaved.event.InputHandler;
import org.fxmisc.wellbehaved.event.InputMap;
import org.fxmisc.wellbehaved.event.Nodes;
import org.reactfx.value.Val;
import org.reactfx.value.Var;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import static com.daslernen.jsonschemaeditor.ui.StageInitialiser.COLOUR_SCHEME_STYLESHEET;
import static javafx.scene.input.KeyCode.F;
import static javafx.scene.input.KeyCombination.ALT_DOWN;
import static javafx.scene.input.KeyCombination.SHORTCUT_DOWN;
import static org.fxmisc.wellbehaved.event.EventPattern.keyPressed;
import static org.fxmisc.wellbehaved.event.InputMap.consume;

final class CodeEditor extends CodeArea implements CodeEditorComponent {

    private static final Logger LOG = LoggerFactory.getLogger(CodeEditor.class);
    private static final ObjectMapper MAPPER = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    private static final String CODE_EDITOR_CSS = "code-editor.css";
    private static final String CODE_EDITOR_STYLESHEET = CodeEditor.class.getResource(CODE_EDITOR_CSS).toExternalForm();

    private final List<Consumer<String>> textChangeListeners = new ArrayList<>();
    private final DoubleProperty fontSize;
    private final Component searchablePane;

    @Inject
    CodeEditor(final EditorConfiguration configuration) {
        fontSize = new SimpleDoubleProperty(configuration.getOpenFile().getFontSize());
        configuration.getOpenFile().fontSizeProperty().bind(fontSize);
        loadEditorFont();
        addFontResizeHotKeys();
        final SearchPane searchPane = new SearchPane(this);
        Nodes.addInputMap(this, InputMap.sequence(
                consume(keyPressed(F, SHORTCUT_DOWN, ALT_DOWN), this::formatCode),
                consume(searchPane.getKeyAction().getEventPattern(), searchPane.getKeyAction()::onEvent)
        ));

        final JsonHighlighter codeHighlighter = new JsonHighlighter();
        getStylesheets().add(codeHighlighter.getStylesheetPath());
        getStylesheets().addAll(COLOUR_SCHEME_STYLESHEET, CODE_EDITOR_STYLESHEET);
        setParagraphGraphicFactory(LineNumberFactory.get(this));
        richChanges()
                .filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
                .subscribe(change -> {
                    final String text = getText();
                    setStyleSpans(0, codeHighlighter.computeHighlighting(text));
                    notifyListeners(text);
                });

        searchablePane = new EditorContentPane(this, searchPane);
    }

    @Override
    public void addTextChangeListener(final Consumer<String> listener) {
        Preconditions.checkNotNull(listener);

        textChangeListeners.add(listener);
    }

    @Override
    public Node getNode() {
        return searchablePane.getNode();
    }

    @Override
    public void moveCaretToStartOfDocument() {
        moveTo(0);
    }

    @Override
    public void replaceText(final String replacement) {
        super.replaceText(replacement);
    }

    private void addFontResizeHotKeys() {
        styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSize.asString(), ";"));
        addEventFilter(ScrollEvent.ANY, event -> {
            if (event.isShortcutDown()) {
                final double change = event.getDeltaY() > 0 ? 1.2 : 0.8;
                fontSize.set(fontSize.multiply(change).getValue());
            }
        });
    }

    private void notifyListeners(final String text) {
        textChangeListeners.forEach(listener -> listener.accept(text));
    }

    private InputHandler.Result formatCode(final Event event) {
        final String code = getText();
        final String formattedCode = formatJson(code);
        if (!Objects.equals(code, formattedCode)) {
            replaceText(formattedCode);
        }
        return InputHandler.Result.PROCEED;
    }

    private String formatJson(String code) {
        try {
            final JsonNode jsonNode = MAPPER.readTree(code);
            return MAPPER.writeValueAsString(jsonNode);
        } catch (IOException e) {
            LOG.info("Couldn't format code: [{}]", e.getMessage().replaceAll("\n", ""));
            return code;
        }
    }

    private void loadEditorFont() {
        final Font editorFont = Font.loadFont(getClass().getResourceAsStream("/fonts/UbuntuMono-R.ttf"), 10);
        Preconditions.checkState(editorFont != null, "Failed to load font");
    }

    private static final class EditorContentPane implements Component, Virtualized {

        private final CodeEditor codeEditor;
        private final AnchorPane pane;

        EditorContentPane(final CodeEditor codeEditor, final Component searchComponent) {
            this.codeEditor = codeEditor;
            pane = layoutComponents(searchComponent.getNode(), new VirtualizedScrollPane<>(codeEditor));
        }

        private AnchorPane layoutComponents(final Node searchField, final Node contentScrollPane) {
            final AnchorPane anchorPane = new AnchorPane(contentScrollPane, searchField);
            AnchorPane.setTopAnchor(searchField, 0.0);
            AnchorPane.setRightAnchor(searchField, 0.0);
            AnchorPane.setTopAnchor(contentScrollPane, 0.0);
            AnchorPane.setRightAnchor(contentScrollPane, 0.0);
            AnchorPane.setBottomAnchor(contentScrollPane, 0.0);
            AnchorPane.setLeftAnchor(contentScrollPane, 0.0);
            return anchorPane;
        }

        @Override
        public Node getNode() {
            return pane;
        }

        @Override
        public Val<Double> totalWidthEstimateProperty() {
            return codeEditor.totalWidthEstimateProperty();
        }

        @Override
        public Val<Double> totalHeightEstimateProperty() {
            return codeEditor.totalHeightEstimateProperty();
        }

        @Override
        public Var<Double> estimatedScrollXProperty() {
            return codeEditor.estimatedScrollXProperty();
        }

        @Override
        public Var<Double> estimatedScrollYProperty() {
            return codeEditor.estimatedScrollYProperty();
        }
    }
}
