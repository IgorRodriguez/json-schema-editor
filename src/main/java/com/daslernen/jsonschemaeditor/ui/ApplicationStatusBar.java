package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.domain.PercentageStringConverter;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.control.Tooltip;

import javax.inject.Inject;

final class ApplicationStatusBar extends MessageBar<Spinner<Double>> {

    private static final int MIN_ZOOM = 100;
    private static final int MAX_ZOOM = 400;
    private static final int DEFAULT_ZOOM = 100;
    private static final int ZOOM_STEP = 25;

    @Inject
    public ApplicationStatusBar(final PercentageStringConverter stringConverter,
                                final ImageLoader imageLoader) {
        super(imageLoader, createScaleSpinner(stringConverter));

        final Spinner<Double> scaleSpinner = getRightNode();
        scaleSpinner.prefWidthProperty().bind(scaleProperty());
        scaleProperty().addListener((o, oldValue, newValue) -> scaleSpinner.getValueFactory().setValue(newValue.doubleValue()));
        scaleSpinner.valueProperty().addListener((o, oldValue, newValue) -> scaleProperty().setValue(newValue));
    }

    private static Spinner<Double> createScaleSpinner(final PercentageStringConverter stringConverter) {
        final DoubleSpinnerValueFactory valueFactory = new DoubleSpinnerValueFactory(MIN_ZOOM, MAX_ZOOM, DEFAULT_ZOOM, ZOOM_STEP);
        valueFactory.setConverter(stringConverter);
        final Spinner<Double> spinner = new Spinner<>(valueFactory);
        spinner.setId("zoom-spinner");
        spinner.setTooltip(new Tooltip("Zoom level"));
        return spinner;
    }
}
