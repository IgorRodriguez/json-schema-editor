package com.daslernen.jsonschemaeditor.ui;

import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.inject.Inject;

public final class StageInitialiser {

    private static final String COLOUR_SCHEME_CSS = "darkula-colour-scheme.css";
    static final String COLOUR_SCHEME_STYLESHEET = CodeEditor.class.getResource(COLOUR_SCHEME_CSS).toExternalForm();

    private final WindowPositionManager windowPositionManager;
    private final RootPane rootPane;

    @Inject
    public StageInitialiser(final WindowPositionManager windowPositionManager, final RootPane rootPane) {
        this.windowPositionManager = windowPositionManager;
        this.rootPane = rootPane;
    }

    public void init(final Stage stage) {
        final Scene scene = new Scene(rootPane.getPane());
        scene.getStylesheets().add(COLOUR_SCHEME_STYLESHEET);
        stage.setTitle("Json Schema Editor");
        stage.setScene(scene);
        windowPositionManager.configure(stage);
        stage.show();
    }
}
