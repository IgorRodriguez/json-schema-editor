package com.daslernen.jsonschemaeditor.ui.action;

import javafx.event.Event;
import javafx.scene.input.KeyEvent;
import org.fxmisc.wellbehaved.event.EventPattern;
import org.fxmisc.wellbehaved.event.InputHandler;

public interface KeyAction {

    EventPattern<Event, KeyEvent> getEventPattern();

    InputHandler.Result onEvent(final KeyEvent event);
}
