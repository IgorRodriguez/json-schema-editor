package com.daslernen.jsonschemaeditor.ui.action;

import javafx.scene.input.KeyEvent;
import org.fxmisc.wellbehaved.event.InputHandler;

public abstract class AbstractKeyAction implements KeyAction {

    protected abstract void execute();

    @Override
    public final InputHandler.Result onEvent(final KeyEvent event) {
        execute();
        return InputHandler.Result.PROCEED;
    }
}
