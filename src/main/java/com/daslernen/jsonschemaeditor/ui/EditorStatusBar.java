package com.daslernen.jsonschemaeditor.ui;

import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

import javax.inject.Inject;

class EditorStatusBar extends MessageBar<Label> {

    private final StringProperty caretPositionProperty;

    @Inject
    EditorStatusBar(final ImageLoader imageLoader) {
        super(imageLoader, createCaretPositionLabel());

        final Label caretPositionLabel = getRightNode();
        caretPositionProperty = caretPositionLabel.textProperty();
    }

    final StringProperty caretPositionPropertyProperty() {
        return caretPositionProperty;
    }

    private static Label createCaretPositionLabel() {
        final Label label = new Label();
        label.setTooltip(new Tooltip("Current position of the caret in the document in the format 'line:character'"));
        return label;
    }
}
