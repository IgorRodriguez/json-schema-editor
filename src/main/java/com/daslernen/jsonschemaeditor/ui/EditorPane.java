package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration;
import com.daslernen.jsonschemaeditor.domain.DocumentHandler;
import com.daslernen.jsonschemaeditor.domain.SaveStatus;
import com.daslernen.jsonschemaeditor.validation.Document;
import com.daslernen.jsonschemaeditor.validation.RawDocument;
import com.daslernen.jsonschemaeditor.validation.Validator;
import com.google.common.base.Preconditions;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.controlsfx.validation.Validator.createPredicateValidator;

final class EditorPane extends VBox {

    private static final double DEFAULT_SCALE = 100;

    private final DoubleProperty scale = new SimpleDoubleProperty(DEFAULT_SCALE);
    private final List<Consumer<RawDocument>> listeners = new ArrayList<>();
    private final EditorStatusBar statusBar;
    private final EditorConfiguration configuration;
    private final Validator validator;
    private final CodeEditorComponent codeEditor;
    private final DocumentHandler documentHandler;

    EditorPane(final EditorConfiguration editorConfiguration,
               final Validator validator,
               final DocumentHandler documentHandler,
               final EditorToolbar toolbar,
               final CodeEditorComponent codeEditor,
               final EditorStatusBar statusBar) {
        this.configuration = editorConfiguration;
        this.validator = validator;
        this.documentHandler = documentHandler;
        this.codeEditor = codeEditor;
        this.statusBar = statusBar;

        codeEditor.addTextChangeListener(this::onCodeChange);
        configuration.pathToRootProperty().addListener(change -> onCodeChange(codeEditor.getText()));
        bindCaretPositionToStatusBarLabel();

        installToolbar(toolbar);
        statusBar.scaleProperty().bindBidirectional(scale);
        toolbar.scaleProperty().bindBidirectional(scale);

        final Node codeEditorNode = codeEditor.getNode();
        getChildren().addAll(toolbar.getNode(), codeEditorNode, statusBar.getNode());
        VBox.setVgrow(codeEditorNode, Priority.ALWAYS);

        setPrefWidth(300);
        setPrefHeight(500);
    }

    void init() {
        configuration.getOpenFile().getContent().ifPresent(this::setEditorContent);
    }

    void addTextChangeListener(final Consumer<RawDocument> listener) {
        Preconditions.checkNotNull(listener);

        listeners.add(listener);
    }

    DoubleProperty scaleProperty() {
        return scale;
    }

    public RawDocument getDocument() {
        return new RawDocument(codeEditor.getText(), configuration.getPathToRoot());
    }

    private void onCodeChange(final String text) {
        configuration.getOpenFile().setContent(text);
        final RawDocument editorDocument = new RawDocument(text, configuration.getPathToRoot());
        setStatusBarMessage(editorDocument);
        listeners.forEach(listener -> listener.accept(editorDocument));
    }

    private void setStatusBarMessage(final RawDocument editorContent) {
        statusBar.setMessages(validator.validate(editorContent));
    }

    private void bindCaretPositionToStatusBarLabel() {
        final ReadOnlyIntegerProperty lineNumberProperty = toProperty(codeEditor.currentParagraphProperty());
        final ReadOnlyIntegerProperty columnNumberProperty = toProperty(codeEditor.caretColumnProperty());
        statusBar.caretPositionPropertyProperty().bind(Bindings.concat(lineNumberProperty.add(1), ":", columnNumberProperty.add(1)));
    }

    private ReadOnlyIntegerProperty toProperty(final ObservableValue<Integer> observable) {
        final SimpleIntegerProperty property = new SimpleIntegerProperty();
        observable.addListener((source, oldValue, newValue) -> property.setValue(newValue));
        return property;
    }

    private void installToolbar(EditorToolbar toolbar) {
        toolbar.setNewAction(this::createNewDocument);
        toolbar.setOpenAction(this::openFile);
        toolbar.setSaveAction(this::saveFile);
        toolbar.setSaveAsAction(this::saveFileAs);
        toolbar.setPathSelectorValidator(createPredicateValidator(Document::isValidPath, "This path doesn't exist in the document"));
    }

    private void createNewDocument() {
        final SaveStatus saveStatus = documentHandler.closeFile(codeEditor.getText());
        if (saveStatus == SaveStatus.SUCCESS) {
            codeEditor.replaceText("");
        }
    }

    private void openFile() {
        documentHandler.openFile(codeEditor.getText()).ifPresent(this::setEditorContent);
    }

    private void saveFile() {
        documentHandler.saveFileWithoutConfirmation(codeEditor.getText());
    }

    private void saveFileAs() {
        documentHandler.saveFileAs(codeEditor.getText());
    }

    private void setEditorContent(final String newContent) {
        codeEditor.replaceText(newContent);
        codeEditor.moveCaretToStartOfDocument();
        codeEditor.getNode().requestFocus();
    }
}
