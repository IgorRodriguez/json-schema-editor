package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.validation.ValidationMessage;
import com.daslernen.jsonschemaeditor.validation.ValidationStatus;
import com.google.common.collect.ImmutableMap;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javaslang.collection.List;

import java.util.Map;
import java.util.SortedSet;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.lang.String.format;

class MessageBar<T extends Node> extends ScalableComponent {

    private static final String CSS_CLASS_NAME = "message-bar";

    private final T rightNode;
    private final Node component;
    private final CollapsiblePane titledPane;

    MessageBar(final ImageLoader imageLoader, final T rightNode) {
        this.rightNode = rightNode;

        titledPane = new CollapsiblePane(createImageMapper(imageLoader));
        component = createComponent(titledPane.getNode(), createContainer(rightNode));
    }

    final void setMessages(final SortedSet<ValidationMessage> messages) {
        titledPane.setMessages(messages);
    }

    @Override
    public final Node getNode() {
        return component;
    }

    final T getRightNode() {
        return rightNode;
    }

    private Node createComponent(final Node mainNode, final Node rightNode) {
        final BorderPane borderPane = new BorderPane(mainNode);
        borderPane.setRight(rightNode);
        borderPane.getStyleClass().add(CSS_CLASS_NAME);
        return borderPane;
    }

    private Map<ValidationStatus, Supplier<ImageView>> createImageMapper(final ImageLoader imageLoader) {
        return ImmutableMap.<ValidationStatus, Supplier<ImageView>>builder()
                .put(ValidationStatus.OK, () -> imageLoader.loadImage("icons/ballgreen.png", scaleProperty()))
                .put(ValidationStatus.WARNING, () -> imageLoader.loadImage("icons/ballorange.png", scaleProperty()))
                .put(ValidationStatus.ERROR, () -> imageLoader.loadImage("icons/ballred.png", scaleProperty()))
                .build();
    }

    private BorderPane createContainer(final T rightNode) {
        final BorderPane borderPane = new BorderPane(rightNode);
        borderPane.setPadding(new Insets(10, 10, 10, 10));
        return borderPane;
    }

    private static final class CollapsiblePane implements Component {

        private final Map<ValidationStatus, Supplier<ImageView>> imageMapper;
        private final TitledPane titledPane;

        CollapsiblePane(final Map<ValidationStatus, Supplier<ImageView>> imageMapper) {
            this.imageMapper = imageMapper;
            titledPane = new AdjustableCollapsiblePane();
            titledPane.setId("collapsible-pane");
        }

        @Override
        public Node getNode() {
            return titledPane;
        }

        final void setMessages(final SortedSet<ValidationMessage> messageSet) {
            final List<ValidationMessage> messages = List.ofAll(messageSet);
            setTitleRow(messages);
            setMessageList(messages);
        }

        private void setTitleRow(final List<ValidationMessage> messages) {
            final ValidationMessage titleMessage = messages.headOption().getOrElse(ValidationMessage.ok());

            final Label label = new Label(titleMessage.getText(), getTitleIcon(titleMessage.getStatus(), messages.size()));
            label.setGraphicTextGap(10);
            label.setTooltip(new Tooltip(titleMessage.getFullText()));
            titledPane.setGraphic(label);
            titledPane.setExpanded(false);
        }

        private Node getTitleIcon(final ValidationStatus status, final int messageCount) {
            final ImageView icon = getIcon(status);
            if (messageCount <= 1) {
                return icon;
            }
            final Label messageCountLabel = new Label(Integer.toString(messageCount));
            messageCountLabel.getStyleClass().add("message-count-overlay");
            return new StackPane(icon, messageCountLabel);
        }

        private void setMessageList(final List<ValidationMessage> messages) {
            if (messages.size() > 1) {
                final GridPane gridPane = new GridPane();
                addMessages(messages.tail(), gridPane);
                addMessageSeparators(gridPane, messages.size() - 2);
                titledPane.setContent(gridPane);
            }
        }

        private void addMessageSeparators(final GridPane gridPane, final int separatorCount) {
            IntStream.range(0, separatorCount)
                    .map(index -> index * 2 + 1)
                    .forEach(rowIndex -> gridPane.add(new Separator(), 0, rowIndex));
        }

        private void addMessages(final List<ValidationMessage> messages, final GridPane gridPane) {
            messages.zipWithIndex()
                    .forEach(tuple -> addToGrid(tuple._1, gridPane, tuple._2));
        }

        private void addToGrid(final ValidationMessage message, final GridPane grid, final Long row) {
            final Label textLabel = new Label(message.getText(), getIcon(message.getStatus()));
            textLabel.setWrapText(true);
            textLabel.setTooltip(new Tooltip(message.getFullText()));
            textLabel.setGraphicTextGap(10);
            grid.add(textLabel, 0, row.intValue() * 2);
        }

        private ImageView getIcon(final ValidationStatus status) {
            final Supplier<ImageView> iconSupplier = imageMapper.get(status);
            if (iconSupplier == null) {
                throw new IllegalArgumentException(format("Icon not found for status [%s]", status));
            }
            return iconSupplier.get();
        }

        private static final class AdjustableCollapsiblePane extends TitledPane {

            @Override
            public Orientation getContentBias() {
                return Orientation.HORIZONTAL;
            }

            @Override
            protected double computeMinWidth(double height) {
                return 0;
            }
        }
    }
}
