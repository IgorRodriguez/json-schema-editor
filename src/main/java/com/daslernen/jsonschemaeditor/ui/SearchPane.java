package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.ui.action.AbstractKeyAction;
import com.daslernen.jsonschemaeditor.ui.action.KeyAction;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import org.controlsfx.control.textfield.CustomTextField;
import org.fxmisc.wellbehaved.event.EventPattern;
import org.fxmisc.wellbehaved.event.InputMap;
import org.fxmisc.wellbehaved.event.Nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static javafx.scene.input.KeyCode.F;
import static javafx.scene.input.KeyCombination.SHORTCUT_DOWN;
import static org.fxmisc.wellbehaved.event.EventPattern.keyPressed;
import static org.fxmisc.wellbehaved.event.InputMap.consume;

final class SearchPane implements Component {

    private static final String PHRASE_NOT_FOUND = "Phrase not found";

    private final Pane searchPanel;
    private final ShowAction showAction;

    SearchPane(final CodeEditor codeEditor) {
        final Label descriptionLabel = createDescriptionLabel();
        final CustomTextField searchField = createSearchField(descriptionLabel);
        final Button closeButton = new Button("⨯");
        final Pane contentPane = createContentPane(searchField, closeButton);
        this.searchPanel = contentPane;

        final AbstractKeyAction searchAction = new SearchAction(codeEditor, searchField, descriptionLabel);
        final HideAction hideAction = new HideAction(codeEditor, contentPane);
        Nodes.addInputMap(searchField, InputMap.sequence(
                consume(searchAction.getEventPattern(), searchAction::onEvent),
                consume(hideAction.getEventPattern(), hideAction::onEvent)
        ));
        closeButton.setOnAction(event -> hideAction.execute());

        showAction = new ShowAction(searchField, contentPane);
    }

    private Label createDescriptionLabel() {
        final Label descriptionLabel = new Label(PHRASE_NOT_FOUND);
        descriptionLabel.setVisible(false);
        return descriptionLabel;
    }

    private CustomTextField createSearchField(final Label descriptionLabel) {
        final CustomTextField searchField = new CustomTextField();
        searchField.setRight(descriptionLabel);
        searchField.setTooltip(new Tooltip("Find occurrences of a text or standard regex in the editor.\nPress Esc to hide"));
        return searchField;
    }

    private Pane createContentPane(final TextField searchField, final Button closeButton) {
        final HBox searchPanel = new HBox(searchField, closeButton);
        searchPanel.setSpacing(5.0);
        searchPanel.getStyleClass().add("editor-search-pane");
        searchPanel.setVisible(false);
        return searchPanel;
    }

    KeyAction getKeyAction() {
        return showAction;
    }

    @Override
    public Node getNode() {
        return searchPanel;
    }

    private static final class ShowAction extends AbstractKeyAction {

        private final TextField searchField;
        private final Pane searchPanel;

        ShowAction(final TextField searchField, final Pane searchPanel) {
            this.searchField = searchField;
            this.searchPanel = searchPanel;
        }

        @Override
        public EventPattern<Event, KeyEvent> getEventPattern() {
            return keyPressed(F, SHORTCUT_DOWN);
        }

        @Override
        protected void execute() {
            searchPanel.setVisible(true);
            searchField.requestFocus();
        }
    }

    private static final class HideAction extends AbstractKeyAction {

        private final CodeEditor codeEditor;
        private final Pane searchPane;

        HideAction(final CodeEditor codeEditor, final Pane searchPane) {
            this.codeEditor = codeEditor;
            this.searchPane = searchPane;
        }

        @Override
        public EventPattern<Event, KeyEvent> getEventPattern() {
            return keyPressed(KeyCode.ESCAPE);
        }

        @Override
        protected void execute() {
            searchPane.setVisible(false);
            codeEditor.requestFocus();
        }
    }

    private static final class SearchAction extends AbstractKeyAction {

        private final CodeEditorComponent codeEditor;
        private final TextField searchField;
        private final Label descriptionLabel;

        SearchAction(final CodeEditorComponent codeEditor, final TextField searchField, final Label descriptionLabel) {
            this.codeEditor = codeEditor;
            this.searchField = searchField;
            this.descriptionLabel = descriptionLabel;
        }

        @Override
        public EventPattern<Event, KeyEvent> getEventPattern() {
            return keyPressed(KeyCode.ENTER);
        }

        @Override
        protected void execute() {
            final List<IndexRange> ranges = getRangesOfMatches();
            if (!ranges.isEmpty()) {
                final int matchIndex = findMatchIndex(ranges);
                selectRange(ranges.get(matchIndex));
                descriptionLabel.setText(String.format("%d of %d matches", matchIndex + 1, ranges.size()));
            } else {
                descriptionLabel.setText(PHRASE_NOT_FOUND);
            }
            descriptionLabel.setVisible(true);
        }

        private List<IndexRange> getRangesOfMatches() {
            final Pattern pattern = Pattern.compile(searchField.getText(), Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
            final Matcher matcher = pattern.matcher(codeEditor.getText());
            final List<IndexRange> ranges = new ArrayList<>();
            while (matcher.find()) {
                ranges.add(new IndexRange(matcher.start(), matcher.end()));
            }
            return ranges;
        }

        /**
         * Finds the index of the first range whose start position is after the caret of the code editor.
         *
         * @param ranges A non-empty list of the ranges of all matches found in the code of the editor
         * @return The list index of the first range after the caret, or the first one if none is found
         */
        private int findMatchIndex(final List<IndexRange> ranges) {
            for (int i = 0; i < ranges.size(); i++) {
                if (ranges.get(i).getStart() >= codeEditor.getCaretPosition()) {
                    return i;
                }
            }
            return 0;
        }

        private void selectRange(final IndexRange range) {
            codeEditor.selectRange(range.getStart(), range.getEnd());
        }
    }
}