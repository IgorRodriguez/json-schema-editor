package com.daslernen.jsonschemaeditor.ui;

import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.controlsfx.control.textfield.TextFields;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import java.nio.file.Path;
import java.util.Optional;

final class EditorToolbar extends ScalableComponent {

    private static final String PATH_SELECTOR_CSS_CLASS_NAME = "file-path-bar";
    private static final String UNSAVED_FILE_PATH = "Not yet saved";

    private final ValidationSupport validationSupport = new ValidationSupport();
    private final VBox component;
    private final Button newButton;
    private final Button openButton;
    private final Button saveButton;
    private final Button saveAsButton;
    private final TextField pathSelector;
    private final String editorTitle;

    EditorToolbar(final EditorConfiguration configuration, final ImageLoader imageLoader) {
        newButton = createNewDocumentButton(imageLoader);
        openButton = createOpenButton(imageLoader);
        saveButton = createSaveButton(imageLoader);
        saveAsButton = createSaveAsButton(imageLoader);
        pathSelector = createPathSelectorField(configuration);
        editorTitle = configuration.getTitle();

        final ToolBar toolBar = new ToolBar(newButton, openButton, saveButton, saveAsButton, new Separator(), addLabel("Path to root: ", pathSelector));
        component = new VBox(createFilePathBar(configuration), toolBar);
    }

    void setNewAction(final Runnable action) {
        newButton.setOnAction(event -> action.run());
    }

    void setOpenAction(final Runnable action) {
        openButton.setOnAction(event -> action.run());
    }

    void setSaveAction(final Runnable action) {
        saveButton.setOnAction(event -> action.run());
    }

    void setSaveAsAction(final Runnable action) {
        saveAsButton.setOnAction(event -> action.run());
    }

    void setPathSelectorValidator(final Validator<String> validator) {
        validationSupport.registerValidator(pathSelector, false, validator);
    }

    @Override
    public final Node getNode() {
        return component;
    }

    private Node addLabel(final String text, final Node node) {
        final Label label = new Label(text);
        label.setLabelFor(node);
        final HBox hbox = new HBox(label, node);
        hbox.setAlignment(Pos.CENTER_LEFT);
        return hbox;
    }

    private Node createFilePathBar(EditorConfiguration configuration) {
        final String initialFilePath = getFilePath(configuration.getOpenFile().getPath());
        final TextField pathField = new TextField(initialFilePath);
        pathField.setFocusTraversable(false);
        pathField.setEditable(false);
        pathField.setMaxWidth(Double.MAX_VALUE);
        pathField.setAlignment(Pos.CENTER);

        pathField.getStyleClass().add(PATH_SELECTOR_CSS_CLASS_NAME);

        configuration.getOpenFile().pathProperty()
                .addListener(((observable, oldValue, newValue) -> setFilePath(pathField, newValue)));
        return new BorderPane(pathField);
    }

    private void setFilePath(TextField pathField, Path newValue) {
        pathField.setText(getFilePath(Optional.ofNullable(newValue)));
    }

    private String getFilePath(final Optional<Path> pathOption) {
        final String pathString = pathOption.map(Object::toString)
                .orElse(UNSAVED_FILE_PATH);
        return editorTitle + ": " + pathString;
    }

    private TextField createPathSelectorField(final EditorConfiguration configuration) {
        final TextField field = TextFields.createClearableTextField();
        field.textProperty().bindBidirectional(configuration.pathToRootProperty());
        field.setTooltip(new Tooltip("The JSON path indicating what part of the document is used with validation purposes.\n"
                + "Leave it empty to indicate that the whole document has to be validated, or (for example) '/body' to indicate that only the 'body' property needs it"));
        return field;
    }

    private Button createSaveButton(final ImageLoader imageLoader) {
        final Button button = new Button(null, imageLoader.loadImage("icons/save2.png", scaleProperty()));
        button.setTooltip(new Tooltip("Save changes in the current document"));
        return button;
    }

    private Button createSaveAsButton(final ImageLoader imageLoader) {
        final Button button = new Button(null, imageLoader.loadImage("icons/saveas.png", scaleProperty()));
        button.setTooltip(new Tooltip("Save changes in the current document to a new file"));
        return button;
    }

    private Button createOpenButton(final ImageLoader imageLoader) {
        final Button button = new Button(null, imageLoader.loadImage("icons/folder.png", scaleProperty()));
        button.setTooltip(new Tooltip("Close the current document and open an existing one"));
        return button;
    }

    private Button createNewDocumentButton(final ImageLoader imageLoader) {
        final Button button = new Button(null, imageLoader.loadImage("icons/document.png", scaleProperty()));
        button.setTooltip(new Tooltip("Close the current document and create a new one"));
        return button;
    }
}
