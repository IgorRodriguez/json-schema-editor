package com.daslernen.jsonschemaeditor.update;

import org.zeroturnaround.zip.NameMapper;
import org.zeroturnaround.zip.ZipUtil;

import java.nio.file.Path;

final class FileUnzipper implements Unzipper {

    @Override
    public void extract(final Path zipPath, final Path destinationPath) {
        unzipFile("current-build", zipPath, destinationPath);
        unzipFile("json-schema-editor.sh", zipPath, destinationPath);
        ZipUtil.unpack(zipPath.toFile(), destinationPath.toFile(), new JarNameMapper());
    }

    private void unzipFile(final String fileName, final Path zipPath, final Path destinationPath) {
        ZipUtil.unpackEntry(zipPath.toFile(), fileName, destinationPath.resolve(fileName).toFile());
    }

    private static final class JarNameMapper implements NameMapper {

        @Override
        public String map(String name) {
            return name.matches(".*json-schema-editor-.*\\.jar") ? name : null;
        }
    }
}
