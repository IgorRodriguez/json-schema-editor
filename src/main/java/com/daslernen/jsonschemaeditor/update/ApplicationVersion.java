package com.daslernen.jsonschemaeditor.update;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import java.util.Objects;
import java.util.regex.Pattern;

final class ApplicationVersion {

    private static final Pattern APPLICATION_VERSION_PATTERN = Pattern.compile("\\d+\\.\\d+\\.\\d{10}");

    private final String value;

    ApplicationVersion(String value) {
        Preconditions.checkArgument(APPLICATION_VERSION_PATTERN.matcher(value).matches(), "Application version [%s] does not match pattern [%s]", value, APPLICATION_VERSION_PATTERN);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplicationVersion that = (ApplicationVersion) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .toString();
    }
}
