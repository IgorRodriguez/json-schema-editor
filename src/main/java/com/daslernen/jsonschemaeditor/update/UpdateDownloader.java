package com.daslernen.jsonschemaeditor.update;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import javaslang.control.Try;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

final class UpdateDownloader {

    static final Path ZIP_FILE_SAVE_PATH = Paths.get("bin");

    private static final Logger LOG = LoggerFactory.getLogger(UpdateDownloader.class);
    private static final String URL = "https://gitlab.com/IgorRodriguez/json-schema-editor/builds/artifacts/master/download?job=package";

    private final OkHttpClient httpClient = new OkHttpClient();
    private final HttpUrl url;

    @Inject
    UpdateDownloader() {
        this(HttpUrl.parse(URL));
    }

    @VisibleForTesting
    UpdateDownloader(final HttpUrl url) {
        this.url = url;
    }

    Try<BuildVersion> getRemoteVersion() {
        return tryToGetRemoteVersion()
                .onFailure(e -> LOG.error("Failed to get remote version", e));
    }

    Try<DownloadResult> downloadLatest() {
        return tryDownload()
                .onFailure(e -> LOG.error("Failed to download updated version", e));
    }

    private Try<BuildVersion> tryToGetRemoteVersion() {
        final Request request = new Request.Builder()
                .head()
                .url(url)
                .build();

        return Try.of( () -> httpClient.newCall(request).execute())
                .filter(this::isOkResponse)
                .map(this::getContentDispositionHeader)
                .map(this::extractBuildVersion);
    }

    private BuildVersion extractBuildVersion(final String fileName) {
        return new BuildVersion(Long.valueOf(extractBuildNumber(fileName)));
    }

    private boolean isOkResponse(final Response response) {
        return response.code() == 200;
    }

    private String getContentDispositionHeader(final Response response) {
        final String fileName = response.headers().get("Content-Disposition");
        if (fileName == null) {
            throw new IllegalStateException("Response does not include the 'Content-Disposition' header");
        }
        return fileName;
    }

    private String extractBuildNumber(final String fileName) {
        try {
            return fileName.substring(fileName.indexOf("_") + 1, fileName.indexOf("."));
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(String.format("Value [%s] does not match the expected file name format", fileName));
        }
    }

    private Try<DownloadResult> tryDownload() {
        final Request request = new Request.Builder()
                .url(url)
                .build();

        return Try.of( () -> httpClient.newCall(request).execute())
                .filter(this::isOkResponse)
                .mapTry(this::downloadFile);
    }

    private DownloadResult downloadFile(final Response response) throws IOException {
        final String remoteFileName = getRemoteAttachmentFileName(response);
        final Path destination = ZIP_FILE_SAVE_PATH.resolve(remoteFileName);
        Files.createDirectories(destination.getParent());
        Files.deleteIfExists(destination);
        Files.createFile(destination);
        try (final BufferedSink sink = Okio.buffer(Okio.sink(destination))) {
            sink.writeAll(response.body().source());
        }
        return new DownloadResult(extractBuildVersion(remoteFileName), destination);
    }

    private String getRemoteAttachmentFileName(final Response response) {
        final String contentHeader = getContentDispositionHeader(response);
        // Header example: Content-Disposition: attachment; filename="json-schema-editor_7267293.zip"
        return contentHeader.substring(contentHeader.indexOf("json-schema-editor_"), contentHeader.indexOf(".zip") + 4);
    }

    static final class DownloadResult {
        private final BuildVersion buildVersion;
        private final Path zipPath;

        DownloadResult(BuildVersion buildVersion, Path zipPath) {
            this.buildVersion = buildVersion;
            this.zipPath = zipPath;
        }

        public BuildVersion getBuildVersion() {
            return buildVersion;
        }

        public Path getZipPath() {
            return zipPath;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("buildVersion", buildVersion)
                    .add("zipPath", zipPath)
                    .toString();
        }
    }
}
