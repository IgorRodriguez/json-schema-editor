package com.daslernen.jsonschemaeditor.update;

import com.daslernen.jsonschemaeditor.service.FileService;

import javax.inject.Inject;
import java.nio.file.Paths;

final class BuildVersionLoader {

    private final FileService fileService;

    @Inject
    public BuildVersionLoader(final FileService fileService) {
        this.fileService = fileService;
    }

    BuildVersion load() {
        return fileService.readString(Paths.get("current-build"))
                .map(String::trim)
                .map(Long::valueOf)
                .map(BuildVersion::new)
                .getOrElse(() -> new BuildVersion(Long.MAX_VALUE));
    }
}
