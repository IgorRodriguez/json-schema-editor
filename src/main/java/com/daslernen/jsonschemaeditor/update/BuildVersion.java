package com.daslernen.jsonschemaeditor.update;

import com.google.common.base.MoreObjects;

import java.util.Objects;

public final class BuildVersion {

    private final long value;

    public BuildVersion(final long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public boolean isMoreRecentThan(final BuildVersion other) {
        return value > other.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BuildVersion that = (BuildVersion) o;
        return value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("value", value)
                .toString();
    }
}
