package com.daslernen.jsonschemaeditor.update;

import java.nio.file.Path;

interface Unzipper {

    void extract(final Path zipPath, final Path destinationPath);
}
