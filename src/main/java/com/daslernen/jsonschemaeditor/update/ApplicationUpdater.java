package com.daslernen.jsonschemaeditor.update;

import javaslang.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ApplicationUpdater {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationUpdater.class);

    private final BuildVersion currentVersion;
    private final UpdateDownloader updateDownloader;
    private final Unzipper unzipper;

    @Inject
    public ApplicationUpdater(final BuildVersion currentVersion, final UpdateDownloader updateDownloader, final Unzipper unzipper) {
        this.currentVersion = currentVersion;
        this.updateDownloader = updateDownloader;
        this.unzipper = unzipper;
    }

    public Try<ApplicationVersion> update() {
        return updateDownloader.getRemoteVersion()
                .filter(remoteVersion -> remoteVersion.isMoreRecentThan(currentVersion))
                .flatMap(v -> updateDownloader.downloadLatest())
                .andThen(result -> unzipper.extract(result.getZipPath(), Paths.get(".")))
                .mapTry(result -> findLatestVersion())
                .onSuccess(appVersion -> LOG.info("Updated application to version [{}]", appVersion.getValue()))
                .onFailure(e -> LOG.error("Failed to update application", e));
    }

    private ApplicationVersion findLatestVersion() throws IOException {
        return Files.list(Paths.get("bin"))
                .map(Path::getFileName)
                .sorted(Comparator.reverseOrder())
                .filter(path -> path.toString().endsWith(".jar"))
                .findFirst()
                .map(this::getApplicationVersion)
                .orElseThrow(() -> new IllegalStateException("Couldn't find installed applications"));
    }

    private ApplicationVersion getApplicationVersion(final Path fileName) {
        final Pattern applicationVersionPattern = Pattern.compile(".*json-schema-editor-(.*)\\.jar");
        final Matcher matcher = applicationVersionPattern.matcher(fileName.toString());
        if (matcher.matches()) {
            return new ApplicationVersion(matcher.group(1));
        }
        throw new IllegalArgumentException(String.format("Path [%s] is not the name of a JAR file", fileName));
    }
}
