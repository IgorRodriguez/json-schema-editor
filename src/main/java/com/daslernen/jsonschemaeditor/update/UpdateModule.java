package com.daslernen.jsonschemaeditor.update;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class UpdateModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Unzipper.class).to(FileUnzipper.class);
    }

    @Provides
    @Singleton
    public BuildVersion getBuild(final BuildVersionLoader buildVersionLoader) {
        return buildVersionLoader.load();
    }
}
