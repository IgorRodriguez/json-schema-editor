package com.daslernen.jsonschemaeditor.domain

import com.google.common.testing.EqualsTester
import junit.framework.AssertionFailedError
import spock.lang.Specification

class NewLineStyleTest extends Specification {

    def "unix new lines are correctly detected"() {
        given:
        def inputText = """"mmmmmmmmmm": 1,\n"iiiiiiiiii":"""

        when:
        def result = NewLineStyle.findFromText(inputText)

        then:
        result.value == "\n"
    }

    def "windows new lines are correctly detected"() {
        given:
        def inputText = "aaaabbbb\r\nbbbb"

        when:
        def result = NewLineStyle.findFromText(inputText)

        then:
        result.value == "\r\n"
    }

    def "system default is used when the input does not contain new line characters"() {
        given:
        def inputText = "aaaabbbb"

        when:
        def result = NewLineStyle.findFromText(inputText)

        then:
        result.value == System.lineSeparator()
    }

    def "test equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(NewLineStyle.findFromText("\naaa"), NewLineStyle.findFromText("b\nbb"))
                .addEqualityGroup(NewLineStyle.findFromText("aa\r\na"), NewLineStyle.findFromText("d\r\nef"))
                .testEquals()

        then:
        notThrown(AssertionFailedError)
    }
}
