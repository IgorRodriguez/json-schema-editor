package com.daslernen.jsonschemaeditor.domain

import spock.lang.Specification

import java.text.NumberFormat

class PercentageStringConverterTest extends Specification {

    def "Percentages are integer numbers followed by percentage symbol ('133%') for UK locale"() {
        given:
        def value = 133
        def underTest = new PercentageStringConverter({ -> NumberFormat.getPercentInstance(Locale.ENGLISH) })

        when:
        def stringValue = underTest.toString(value)

        then:
        stringValue == '133%'
    }
}
