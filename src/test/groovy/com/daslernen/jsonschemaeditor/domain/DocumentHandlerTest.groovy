package com.daslernen.jsonschemaeditor.domain

import com.daslernen.jsonschemaeditor.configuration.EditorConfiguration
import com.daslernen.jsonschemaeditor.service.FileService
import javaslang.control.Try
import spock.lang.Specification

import java.nio.file.Path
import java.nio.file.Paths

class DocumentHandlerTest extends Specification {

    def fileService = Mock(FileService)
    def dialogProvider = Mock(DialogProvider)

    def "openFile 2: not file yet open and no content passed, when file fails to open returns empty optional"() {
        given:
        def filePath = Paths.get("/test/doc1.json")
        dialogProvider.showOpenDialog(_) >> Optional.of(filePath)
        fileService.readString(filePath) >> Try.failure(new IOException("test file doesn't exist"))
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def openFileContent = underTest.openFile("")

        then:
        !openFileContent.isPresent()
    }

    def "openFile 3: not file yet open and no content passed, when user cancels opening dialog returns empty optional"() {
        given:
        dialogProvider.showOpenDialog(_) >> Optional.empty()
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def openFileContent = underTest.openFile("")

        then:
        !openFileContent.isPresent()
    }

    def "openFile 4: not file yet open and some content passed, when user saves file and open new one returns its content"() {
        given:
        def pathToOpen = Paths.get("/test/doc1.json")
        def pathToSave = Paths.get("/test/destination/doc2.json")
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.SAVE
        dialogProvider.showSaveDialog(_) >> Optional.of(pathToSave)
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.save(pathToSave, _) >> Try.success(pathToSave)
        fileService.readString(pathToOpen) >> Try.success("this-is-the-content-of-the-file")
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def openFileContent = underTest.openFile("some-changes")

        then:
        openFileContent.get() == "this-is-the-content-of-the-file"
    }

    def "openFile 5: not file yet open and some content passed, when an error occurs saving the file returns empty optional"() {
        given:
        def pathToSave = Paths.get("/test/destination/doc2.json")
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.SAVE
        dialogProvider.showSaveDialog(_) >> Optional.of(pathToSave)
        fileService.save(pathToSave, _) >> Try.failure(new IOException("test-save-error"))
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def openFileContent = underTest.openFile("some-changes")

        then:
        0 * fileService.readString(_)
        !openFileContent.isPresent()
    }

    def "openFile 6: not file yet open and some content passed, when user cancels saving returns empty optional"() {
        given:
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.CANCEL
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def openFileContent = underTest.openFile("some-changes")

        then:
        0 * fileService.save(_, _)
        0 * fileService.readString(_)
        !openFileContent.isPresent()
    }

    def "openFile 7: not file yet open and some content passed, when user rejects saving returns the content of the opened file"() {
        given:
        def pathToOpen = Paths.get("/test/doc1.json")
        def contentOfFileToOpen = "this-is-the-content-of-the-file"
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.DO_NOT_SAVE
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.readString(pathToOpen) >> Try.success(contentOfFileToOpen)
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def openFileContent = underTest.openFile("some-changes")

        then:
        0 * fileService.save(_, _)
        openFileContent.get() == contentOfFileToOpen
        underTest.state.content == contentOfFileToOpen
    }

    def "openFile 8: file open and original content passed, returns the opened file"() {
        given:
        def pathToOpen = Paths.get("/test/doc1.json")
        def contentOfFileToOpen = "this-is-the-content-of-the-file"
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.readString(pathToOpen) >> Try.success(contentOfFileToOpen)
        def underTest = createDocumentHandlerWithOpenFile(Paths.get("/some/path"), "same-content")

        when:
        def openFileContent = underTest.openFile("same-content")

        then:
        0 * dialogProvider.showSaveConfirmationAlert(_, _)
        0 * fileService.save(_, _)
        openFileContent.get() == contentOfFileToOpen
        underTest.state.content == contentOfFileToOpen
    }

    def "openFile 9: file open and different content passed, when the user saves the changes returns the opened file"() {
        given:
        def pathToOpen = Paths.get("/test/doc1.json")
        def pathToSave = Paths.get("/test/destination/doc2.json")
        def contentOfFileToOpen = "this-is-the-content-of-the-file"
        def changedContent = "some-changed-content"
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.SAVE
        dialogProvider.showSaveDialog(_) >> Optional.of(pathToSave)
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.save(pathToSave, changedContent) >> Try.success(pathToSave)
        fileService.readString(pathToOpen) >> Try.success(contentOfFileToOpen)
        def underTest = createDocumentHandlerWithOpenFile(pathToSave, "some-content")

        when:
        def openFileContent = underTest.openFile(changedContent)

        then:
        openFileContent.get() == contentOfFileToOpen
        underTest.state.content == contentOfFileToOpen
    }

    def "openFile 10: file open and different content passed, when the cancels saving returns empty optional"() {
        given:
        def pathToSave = Paths.get("/test/destination/doc2.json")
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.CANCEL
        def underTest = createDocumentHandlerWithOpenFile(pathToSave, "some-content")

        when:
        def openFileContent = underTest.openFile("some-changed-content")

        then:
        0 * dialogProvider.showSaveDialog(_)
        0 * dialogProvider.showOpenDialog(_)
        0 * fileService.save(_, _)
        0 * fileService.readString(_)
        !openFileContent.isPresent()
    }

    def "openFile 11: file open and different content passed, when the user rejects saving changes the opened file"() {
        given:
        def pathToOpen = Paths.get("/test/doc1.json")
        def pathToSave = Paths.get("/test/destination/doc2.json")
        def contentOfFileToOpen = "this-is-the-content-of-the-file"
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.DO_NOT_SAVE
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.readString(pathToOpen) >> Try.success(contentOfFileToOpen)
        def underTest = createDocumentHandlerWithOpenFile(pathToSave, "some-content")

        when:
        def openFileContent = underTest.openFile("some-changed-content")

        then:
        0 * dialogProvider.showSaveDialog(_)
        0 * fileService.save(_, _)
        openFileContent.get() == contentOfFileToOpen
        underTest.state.content == contentOfFileToOpen
    }

    def "openFile 12: when file is open, changed, saved and openFile invoked no save confirmation alert is displayed"() {
        given:
        def filePath = Paths.get("/test/doc1.json")
        def changedContent = "some-changed-content"
        fileService.exists(filePath) >> true
        fileService.readString(_) >> Try.success("content1") >> Try.success("content2")
        fileService.save(filePath, changedContent) >> Try.success(filePath)
        dialogProvider.showOpenDialog(_) >> Optional.of(Paths.get("/test/doc1.json")) >> Optional.of(Paths.get("/some/path"))
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        underTest.openFile("")
        underTest.saveFileWithoutConfirmation(changedContent)
        def openFileContent = underTest.openFile(changedContent)

        then:
        0 * dialogProvider.showSaveConfirmationAlert(_, _)
        0 * dialogProvider.showSaveDialog(_)
        openFileContent.get() == "content2"
    }

    def "closeFile: when no file is open and content is empty returns success without saving the file"() {
        given:
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def saveStatus = underTest.closeFile("")

        then:
        saveStatus == SaveStatus.SUCCESS
        0 * fileService.save(_, _)
        0 * dialogProvider.showSaveConfirmationAlert(_, _)
    }

    def "closeFile: when no file is open and content is whitespace returns success without saving the file"() {
        given:
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def saveStatus = underTest.closeFile("    \t   \n \r\n")

        then:
        saveStatus == SaveStatus.SUCCESS
        0 * fileService.save(_, _)
        0 * dialogProvider.showSaveConfirmationAlert(_, _)
    }

    def "closeFile: when no file is open and content has non-whitespace characters returns success after saving the file"() {
        given:
        def changedContent = "aaaa"
        def pathToSave = Paths.get("/some/test/file.txt")
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.SAVE
        dialogProvider.showSaveDialog(_) >> Optional.of(pathToSave)
        fileService.save(pathToSave, changedContent) >> Try.success(pathToSave)
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)

        when:
        def saveStatus = underTest.closeFile(changedContent)

        then:
        saveStatus == SaveStatus.SUCCESS
        dialogProvider.showSaveConfirmationAlert(_ as String, _ as String)
        fileService.save(pathToSave, changedContent)
    }

    def "closeFile: when a file is open and content hasn't changed returns success without saving the file"() {
        given:
        def content = "some-content"
        def pathToOpen = Paths.get("/some/file.txt")
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.readString(pathToOpen) >> Try.success(content)
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)


        when:
        underTest.openFile("")
        def saveStatus = underTest.closeFile(content)

        then:
        saveStatus == SaveStatus.SUCCESS
        0 * fileService.save(_, _)
        0 * dialogProvider.showSaveConfirmationAlert(_, _)
    }

    def "closeFile: when a file is open and content has changed returns success after saving the file"() {
        given:
        def changedContent = "changed-content"
        def pathToOpen = Paths.get("/some/file.txt")
        dialogProvider.showOpenDialog(_) >> Optional.of(pathToOpen)
        fileService.readString(pathToOpen) >> Try.success("some-content")
        dialogProvider.showSaveConfirmationAlert(_, _) >> SaveConfirmation.SAVE
        fileService.save(pathToOpen, changedContent) >> Try.success(pathToOpen)
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)


        when:
        underTest.openFile("")
        def saveStatus = underTest.closeFile(changedContent)

        then:
        saveStatus == SaveStatus.SUCCESS
        fileService.save(pathToOpen, changedContent)
        0 * dialogProvider.showSaveDialog(_)
    }

    def "saveFileWithoutConfirmation: when a file is open and changes are passed the file is saved with requesting confirmation"() {
        given:
        def changedContent = "some-changes"
        def filePath = Paths.get("/path/to/file.txt")
        dialogProvider.showOpenDialog(_) >> Optional.of(filePath)
        fileService.readString(filePath) >> Try.success("some-content")
        fileService.save(filePath, changedContent) >> Try.success(filePath)
        def underTest = new DocumentHandler(new EditorConfiguration(), dialogProvider, fileService)


        when:
        underTest.openFile("")
        underTest.saveFileWithoutConfirmation(changedContent)

        then:
        fileService.save(filePath, changedContent)
        0 * dialogProvider.showSaveConfirmationAlert(_, _)
    }

    def "saveAs: when the user select a file to save to the file is saved and the open file in the configuration is updated"() {
        given:
        def editorContent = "some-content"
        def destinationPath = Paths.get("/path/to/destination.txt")
        dialogProvider.showSaveDialog(_) >> Optional.of(destinationPath)
        fileService.save(destinationPath, editorContent) >> Try.success(destinationPath)
        def configuration = new EditorConfiguration()
        def underTest = new DocumentHandler(configuration, dialogProvider, fileService)

        when:
        underTest.saveFileAs(editorContent)

        then:
        configuration.openFile.content.get() == editorContent
        configuration.openFile.path.get() == destinationPath
    }

    def "saveAs: when the user cancels the save dialog the file is not saved and the configuration doesn't change"() {
        given:
        def originalPath = Paths.get("original/path")
        def editorContent = "some-content"
        def destinationPath = Paths.get("/path/to/destination.txt")
        dialogProvider.showSaveDialog(_) >> Optional.empty()
        def configuration = configFor(originalPath, "abc")
        def underTest = new DocumentHandler(configuration, dialogProvider, fileService)

        when:
        underTest.saveFileAs(editorContent)

        then:
        0 * fileService.save(_, _)
        configuration.openFile.content.get() == "abc"
        configuration.openFile.path.get() == originalPath
    }

    def createDocumentHandlerWithOpenFile(Path path, String content) {
        def configuration = configForPath(path)
        def documentHandler = new DocumentHandler(configuration, dialogProvider, fileService)
        documentHandler.stateHolder.state = new CurrentFile(path, content)
        documentHandler
    }

    def configFor(Path path, String content) {
        def configuration = new EditorConfiguration()
        def file = configuration.getOpenFile()
        file.setPath(path)
        file.setContent(content)
        configuration
    }

    def configForPath(Path path) {
        configFor(path, "abc")
    }
}
