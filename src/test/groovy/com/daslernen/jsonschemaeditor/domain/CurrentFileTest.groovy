package com.daslernen.jsonschemaeditor.domain

import com.google.common.testing.EqualsTester
import junit.framework.AssertionFailedError
import spock.lang.Specification

import static java.nio.file.Paths.get

class CurrentFileTest extends Specification {

    static final NewLineStyle UNIX_STYLE = NewLineStyle.findFromText("\n")
    static final NewLineStyle WINDOWS_STYLE = NewLineStyle.findFromText("\r\n")

    def "cannot pass null as an argument for content"() {
        when:
        new CurrentFile(get("/some/path"), null)

        then:
        thrown(NullPointerException)
    }

    def "OS dependent new line style is calculated from content"() {
        given:
        def path = get("/a")

        when:
        def openFile = new CurrentFile(path, content)

        then:
        openFile.getPath() == path
        openFile.getContent() == content
        openFile.getNewLineStyle().getValue() == newLineStyleValue

        where:
        content|newLineStyleValue
        "aaa\nb"| "\n"
        "aaa\r\nb"|"\r\n"
    }

    def "test equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new CurrentFile(get("/aa"), "aaa", UNIX_STYLE), new CurrentFile(get("/aa"), "aaa", UNIX_STYLE))
                .addEqualityGroup(new CurrentFile(get("/bb"), "bbb", UNIX_STYLE), new CurrentFile(get("/bb"), "bbb", UNIX_STYLE))
                .addEqualityGroup(new CurrentFile(get("/aa"), "aaa", WINDOWS_STYLE), new CurrentFile(get("/aa"), "aaa", WINDOWS_STYLE))
                .testEquals()

        then:
        notThrown(AssertionFailedError)
    }
}
