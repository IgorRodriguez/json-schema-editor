package com.daslernen.jsonschemaeditor.update

import com.daslernen.jsonschemaeditor.service.FileService
import javaslang.control.Try
import spock.lang.Specification

import java.nio.file.Paths

class BuildVersionLoaderTest extends Specification {

    def "build version is loaded from 'current-build' file"() {
        given:
        def fileService = Mock(FileService)
        fileService.readString(Paths.get("current-build")) >> Try.success("1234")
        def underTest = new BuildVersionLoader(fileService)

        when:
        def buildVersion = underTest.load()

        then:
        buildVersion.value == 1234
    }

    def "max long value is returned when build version cannot be loaded from file"() {
        given:
        def fileService = Mock(FileService)
        fileService.readString(Paths.get("current-build")) >> Try.failure(new IOException("test"))
        def underTest = new BuildVersionLoader(fileService)

        when:
        def buildVersion = underTest.load()

        then:
        buildVersion.value == Long.MAX_VALUE
    }
}
