package com.daslernen.jsonschemaeditor.update

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Paths

class UpdateDownloaderTest extends Specification {

    def remoteServer = new MockWebServer()
    def savePath = Paths.get("bin/json-schema-editor_8888.zip")

    def setup() {
        Files.deleteIfExists(savePath)
    }

    def cleanup() {
        Files.deleteIfExists(savePath)
    }

    def "getRemoteVersion: build version from server is retrieved"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader("Content-Disposition", "json-schema-editor_8888.zip"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryVersion = underTest.getRemoteVersion()

        then:
        tryVersion.get() == new BuildVersion(8888L)

        cleanup:
        remoteServer.shutdown()
    }

    def "getRemoteVersion: a failure is returned when there's an error calling the remote server"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(404)
                .setHeader("Content-Disposition", "json-schema-editor_8888.zip"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryVersion = underTest.getRemoteVersion()

        then:
        tryVersion.isFailure()

        cleanup:
        remoteServer.shutdown()
    }

    def "getRemoteVersion: a failure is returned if the request has a no 'Content-Disposition' header"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(200))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryVersion = underTest.getRemoteVersion()

        then:
        tryVersion.isFailure()

        cleanup:
        remoteServer.shutdown()
    }

    def "getRemoteVersion: a failure is returned if the value of 'Content-Disposition' does not match the expected format"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader("Content-Disposition", "blah"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryVersion = underTest.getRemoteVersion()

        then:
        tryVersion.isFailure()

        cleanup:
        remoteServer.shutdown()
    }

    def "downloadLatest: file is downloaded from the remote server"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader("Content-Disposition", "attachment; filename=\"json-schema-editor_8888.zip\"")
                .setBody("some-file-content"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryDownloadResult = underTest.downloadLatest()

        then:
        savePath.text == "some-file-content"
        tryDownloadResult.get().buildVersion == new BuildVersion(8888L)
        tryDownloadResult.get().zipPath == Paths.get("bin/json-schema-editor_8888.zip")

        cleanup:
        remoteServer.shutdown()
    }

    def "downloadLatest: a failure is returned when response code from server is not 200"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(500)
                .setHeader("Content-Disposition", "attachment; filename=\"json-schema-editor_8888.zip\"")
                .setBody("some-file-content"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryDownloadResult = underTest.downloadLatest()

        then:
        tryDownloadResult.isFailure()

        cleanup:
        remoteServer.shutdown()
    }

    def "downloadLatest: a failure is returned if the response does not include the 'Content-Disposition' header"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody("some-file-content"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryDownloadResult = underTest.downloadLatest()

        then:
        tryDownloadResult.isFailure()

        cleanup:
        remoteServer.shutdown()
    }

    def "downloadLatest: a failure is returned if the 'Content-Disposition' does not include the expected file name"() {
        given:
        remoteServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader("Content-Disposition", "some-file.zip")
                .setBody("some-file-content"))
        remoteServer.start()
        def url = remoteServer.url("/test/destination")
        def underTest = new UpdateDownloader(url)

        when:
        def tryDownloadResult = underTest.downloadLatest()

        then:
        tryDownloadResult.isFailure()

        cleanup:
        remoteServer.shutdown()
    }
}
