package com.daslernen.jsonschemaeditor.update

import com.google.common.testing.EqualsTester
import spock.lang.Specification

class BuildVersionTest extends Specification {

    def "false is returned when both versions have equal value"() {
        given:
        def underTest = new BuildVersion(123)
        def other = new BuildVersion(123)

        when:
        def isMoreRecent = underTest.isMoreRecentThan(other)

        then:
        !isMoreRecent
    }

    def "true is returned when other version has a lower value"() {
        given:
        def underTest = new BuildVersion(123)
        def other = new BuildVersion(1)

        when:
        def isMoreRecent = underTest.isMoreRecentThan(other)

        then:
        isMoreRecent
    }

    def "false is returned when other version has a higher value"() {
        given:
        def underTest = new BuildVersion(123)
        def other = new BuildVersion(555)

        when:
        def isMoreRecent = underTest.isMoreRecentThan(other)

        then:
        !isMoreRecent
    }

    def "implements equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new BuildVersion(123), new BuildVersion(123))
                .addEqualityGroup(new BuildVersion(333), new BuildVersion(333))
                .testEquals()

        then:
        noExceptionThrown()
    }
}
