package com.daslernen.jsonschemaeditor.update

import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class FileUnzipperTest extends Specification {

    def "files are extracted from zip file and saved in the appropriate destinations"() {
        given:
        def testDirectory = Paths.get("test-tmp")
        def zipPath = testDirectory.resolve("test-update.zip")
        givenZipFile(zipPath)
        def underTest = new FileUnzipper()

        when:
        underTest.extract(zipPath, testDirectory)

        then:
        Paths.get("test-tmp/current-build").text == "test build id"
        Paths.get("test-tmp/json-schema-editor.sh").text == "test sh file"
        Paths.get("test-tmp/bin/json-schema-editor-123.jar").text == "test jar"

        cleanup:
        Files.deleteIfExists(zipPath)
        if (Files.exists(testDirectory)) {
            Files.walk(testDirectory)
                    .sorted(Comparator.reverseOrder())
                    .forEach({ path -> Files.deleteIfExists(path) })
        }
        Files.deleteIfExists(testDirectory)
    }

    private void givenZipFile(Path zipDestinationPath) {
        Files.createDirectories(zipDestinationPath.getParent())
        def testFileUrl = getClass().getClassLoader().getResource("com/daslernen/jsonschemaeditor/update/test-update.zip")
        Files.copy(Paths.get(testFileUrl.getPath()), zipDestinationPath)
    }
}