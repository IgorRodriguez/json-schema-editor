package com.daslernen.jsonschemaeditor.update

import com.google.common.testing.EqualsTester
import spock.lang.Specification

class ApplicationVersionTest extends Specification {

    def "can create an instance with a version with format #majorVersion.#minorVersion.#buildTime"() {
        def versionString = "1.2.0123456789"
        when:
        def version = new ApplicationVersion(versionString)

        then:
        version.getValue() == versionString
    }

    def "cannot create an instance when there are not three digit groups"() {
        when:
        new ApplicationVersion("1.2")

        then:
        thrown(IllegalArgumentException)
    }

    def "cannot create an instance when there are letters"() {
        when:
        new ApplicationVersion("1.2.abcdefghij")

        then:
        thrown(IllegalArgumentException)
    }

    def "cannot create an instance when last group has less than 10 digit's length"() {
        when:
        new ApplicationVersion("1.2.012345678")

        then:
        thrown(IllegalArgumentException)
    }

    def "cannot create an instance when last group has more than 10 digit's length"() {
        when:
        new ApplicationVersion("1.2.01234567890")

        then:
        thrown(IllegalArgumentException)
    }

    def "implements equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new ApplicationVersion("1.1.1111111111"), new ApplicationVersion("1.1.1111111111"))
                .addEqualityGroup(new ApplicationVersion("22.1.1111111111"), new ApplicationVersion("22.1.1111111111"))
                .addEqualityGroup(new ApplicationVersion("1.22.1111111111"), new ApplicationVersion("1.22.1111111111"))
                .addEqualityGroup(new ApplicationVersion("1.1.2222222222"), new ApplicationVersion("1.1.2222222222"))
                .testEquals()

        then:
        noExceptionThrown()
    }
}
