package com.daslernen.jsonschemaeditor.validation

import spock.lang.Specification

import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.error
import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.ok
import static com.daslernen.jsonschemaeditor.validation.ValidationVisitor.Result.*

class ValidationVisitorTest extends Specification {

    private final ValidationVisitor<String> underTest = ValidationVisitor.create({ -> okResult("some-value") })

    def "a new instance contains a result with both value and message"() {
        when:
        def visitor = ValidationVisitor.create({ -> okResult("some-value") })

        then:
        visitor.messages.isEmpty()
        visitor.result.value.get() == "some-value"
    }

    def "supplier for new instance cannot return a null value"() {
        when:
        ValidationVisitor.create({ -> null })

        then:
        thrown NullPointerException
    }

    def "map creates a new instance whose value is the result returned by the passed function"() {
        when:
        def visitor = underTest.map({ value -> result([error("test-failure")] as Set, 25) })

        then:
        visitor.messages.size() == 1
        visitor.messages[0].status == ValidationStatus.ERROR
        visitor.messages[0].text == "test-failure"
        visitor.result.value.get() == 25
    }

    def "map does not accept null"() {
        when:
        underTest.map({ value -> null })

        then:
        thrown NullPointerException
    }

    def "results allow value to be null"() {
        when:
        result([ok()] as Set, null)

        then:
        noExceptionThrown()
    }

    def "results do not allow the message to be null"() {
        when:
        result(null, 25)

        then:
        thrown NullPointerException
    }

    def "when the supplier of a new instance throws an exception a failure message is created"() {
        when:
        def visitor = ValidationVisitor.create({ -> throw new UnsupportedOperationException("test") })

        then:
        visitor.messages.size() == 1
        visitor.messages[0].status == ValidationStatus.ERROR
        visitor.messages[0].text == "Internal error caused by [java.lang.UnsupportedOperationException: test]"
        !visitor.result.value.isPresent()
    }

    def "when the mapping function throws an exception a failure message is created"() {
        when:
        def visitor = underTest.map({ value -> throw new UnsupportedOperationException("test") })

        then:
        visitor.messages.size() == 1
        visitor.messages[0].status == ValidationStatus.ERROR
        visitor.messages[0].text == "Internal error caused by [java.lang.UnsupportedOperationException: test]"
        !visitor.result.value.isPresent()
    }

    def "after an error no more functions are executed"() {
        given:
        def tempVisitor = underTest.map({ value -> errorResult("not-valid") })

        when:
        def visitor = tempVisitor.map({ value -> okResult(11) })

        then:
        visitor.messages.size() == 1
        visitor.messages[0].status == ValidationStatus.ERROR
        visitor.messages[0].text == "not-valid"
        !visitor.result.value.isPresent()
    }

    def "value of passed visitor is merged with existing one"() {
        given:
        def visitorToMerge = ValidationVisitor.create({ -> okResult("value") })

        when:
        def mergedVisitor = underTest.merge(visitorToMerge, { value1, value2 -> value1.length() + value2.length() })

        then:
        mergedVisitor.messages.isEmpty()
        mergedVisitor.result.value.get() == 15
    }

    def "when merging a visitor with error message with a successful one only the error message and no value are returned"() {
        given:
        def visitor1 = ValidationVisitor.create({ -> okResult("value1") })
        def visitor2 = ValidationVisitor.create({ -> errorResult("message2") })

        when:
        def mergedVisitor = visitor1.merge(visitor2, { value1, value2 -> value1 + value2 })

        then:
        mergedVisitor.messages as List == [error("message2")]
        !mergedVisitor.result.value.isPresent()
    }

    def "when merging a visitor with error message with another failing one the original one prevails and resulting value is null"() {
        given:
        def visitor1 = ValidationVisitor.create({ -> errorResult("message1") })
        def visitor2 = ValidationVisitor.create({ -> errorResult("message2") })

        when:
        def mergedVisitor = visitor1.merge(visitor2, { value1, value2 -> value1 + value2 })

        then:
        mergedVisitor.messages as List == [error("message1"), error("message2")]
        !mergedVisitor.result.value.isPresent()
    }

    def "merge results in error message when both visitors are successful but mapping function throws an exception"() {
        given:
        def failingMappingFunction = {value1, value2 -> throw new UnsupportedOperationException("test")}
        def visitor1 = ValidationVisitor.create({ -> okResult("value1") })

        when:
        def mergedVisitor = underTest.merge(visitor1, failingMappingFunction)

        then:
        mergedVisitor.messages as List == [error("Failed to combine [some-value] with [value1]")]
        !mergedVisitor.result.value.isPresent()
    }


}
