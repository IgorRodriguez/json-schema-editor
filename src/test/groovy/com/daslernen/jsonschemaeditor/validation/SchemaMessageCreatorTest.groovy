package com.daslernen.jsonschemaeditor.validation

import com.daslernen.jsonschemaeditor.domain.JsonMapper
import com.github.fge.jsonschema.main.JsonSchemaFactory
import spock.lang.Specification

import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.error
import static com.daslernen.jsonschemaeditor.validation.ValidationMessage.warn

class SchemaMessageCreatorTest extends Specification {

    def jsonMapper = new JsonMapper()
    def underTest = new SchemaMessageCreator()

    def "when pointer is available it is returned"() {
        given:
        def schema = """
        {
          "properties" : {
            "firstName" : {
              "type" : null
            }
          }
        }
        """
        def report = JsonSchemaFactory.byDefault().getSyntaxValidator().validateSchema(jsonMapper.nodeFromString(schema))

        when:
        def messages = underTest.toMessages(report)

        then:
        messages == [error("[/properties/firstName] value has incorrect type (found null, expected one of [array, string])")] as Set
    }

    def "returned messages are sorted by error level"() {
        given:
        def schema = """
        {
          "note" : "some note",
          "properties" : {
            "firstName" : {
              "type" : null
            }
          }
        }
        """
        def report = JsonSchemaFactory.byDefault().getSyntaxValidator().validateSchema(jsonMapper.nodeFromString(schema))

        when:
        def messages = underTest.toMessages(report)

        then:
        messages as List == [error("[/properties/firstName] value has incorrect type (found null, expected one of [array, string])"),
                             warn("the following keywords are unknown and will be ignored: [note]")]
    }
}
