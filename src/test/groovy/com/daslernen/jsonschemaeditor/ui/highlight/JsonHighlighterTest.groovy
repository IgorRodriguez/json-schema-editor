package com.daslernen.jsonschemaeditor.ui.highlight

import org.fxmisc.richtext.StyleSpan
import spock.lang.Specification

class JsonHighlighterTest extends Specification {

    final def underTest = new JsonHighlighter()

    def "style sheet exists"() {
        when:
        def path = underTest.getStylesheetPath()

        then:
        path != null
    }

    def "highlighting works on empty strings"() {
        when:
        def styleSpans = underTest.computeHighlighting("")

        then:
        def spans = styleSpans.asList()
        spans.size() == 1
        spans.get(0) == emptyStyle(0)
    }

    def "the appropriate styles are created"() {
        when:
        def styleSpans = underTest.computeHighlighting("""{ "aaa": [ 1, 2 ] }""")

        then:
        def spans = styleSpans.asList()
        spans == [
                style("punctuation", 2),
                style("property", 5),
                style("punctuation", 4),
                style("number", 1),
                style("punctuation", 2),
                style("number", 2),
                style("punctuation", 3)]
    }

    def "curly and square brackets, commas and colons use 'punctuation' style"() {
        when:
        def styleSpans = underTest.computeHighlighting("""{}[],:""")

        then:
        def spans = styleSpans.asList()
        spans.size() == 1
        spans.get(0) == style("punctuation", 6)
    }

    def "strings use 'string' style"() {
        when:
        def styleSpans = underTest.computeHighlighting("\"some-value\"")

        then:
        def spans = styleSpans.asList()
        spans.size() == 1
        spans.get(0) == style("string", 12)
    }

    def "numbers use 'number' style"() {
        when:
        def styleSpans = underTest.computeHighlighting("123, 12.3")

        then:
        styleSpans.asList() == [
                style("number", 3),
                style("punctuation", 2),
                style("number", 4)]
    }

    def "'null', 'true' and 'false' use 'propertyvalue' style"() {
        when:
        def styleSpans = underTest.computeHighlighting("null,true,false")

        then:
        def spans = styleSpans.asList()
        spans.size() == 5
        spans.get(0) == style("propertyvalue", 4)
        spans.get(1) == style("punctuation", 1)
        spans.get(2) == style("propertyvalue", 4)
        spans.get(3) == style("punctuation", 1)
        spans.get(4) == style("propertyvalue", 5)
    }

    def "properties use 'property' style"() {
        when:
        def styleSpans = underTest.computeHighlighting("\"a-property\" : \"a-value\"")

        then:
        styleSpans.asList() == [
                style("property", 13),
                style("punctuation", 2),
                style("string", 9)]
    }

    def style(String name, int length) {
        new StyleSpan(Collections.singleton(name), length)
    }

    def emptyStyle(int length) {
        new StyleSpan(Collections.emptySet(), length)
    }
}
