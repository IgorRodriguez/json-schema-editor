package com.daslernen.jsonschemaeditor.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.testing.EqualsTester
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

class ConfigurationTest extends Specification {

    private def objectMapper = new ObjectMapper()

    def "implements equals and hashcode"() {
        given:
        def windowProperties1 = Mock(WindowProperties)
        def windowProperties2 = Mock(WindowProperties)
        def editorConfig1 = Mock(EditorConfiguration)
        def editorConfig2 = Mock(EditorConfiguration)
        def otherEditorConfig = Mock(EditorConfiguration)

        when:
        new EqualsTester()
                .addEqualityGroup(new Configuration(), new Configuration())
                .addEqualityGroup(create(windowProperties1, editorConfig1, editorConfig2), create(windowProperties1, editorConfig1, editorConfig2))
                .addEqualityGroup(create(windowProperties2, editorConfig1, editorConfig2), create(windowProperties2, editorConfig1, editorConfig2))
                .addEqualityGroup(create(windowProperties1, otherEditorConfig, editorConfig2), create(windowProperties1, otherEditorConfig, editorConfig2))
                .addEqualityGroup(create(windowProperties1, editorConfig1, otherEditorConfig), create(windowProperties1, editorConfig1, otherEditorConfig))
                .testEquals()
        then:
        noExceptionThrown()
    }

    def "can serialise to json"() {
        given:
        def instance = create(null, null, null)

        when:
        def json = objectMapper.writeValueAsString(instance)

        then:
        def expected = """{ "windowProperties": null, "jsonEditor": null, "schemaEditor": null }"""
        JSONAssert.assertEquals(expected, json, true)
    }

    def "can deserialise from json"() {
        given:
        def json = """{ 
                    "windowProperties": {
                        "x": 123
                    }, 
                    "jsonEditor": {
                        "title": "some-json-editor"
                    }, 
                    "schemaEditor": {
                        "title": "some-schema-editor"
                    } 
                }"""

        when:
        def instance = objectMapper.readValue(json, Configuration)

        then:
        def expected = new Configuration()
                .setWindowProperties(new WindowProperties()
                        .setX(123))
                .setJsonEditor(new EditorConfiguration()
                        .setTitle("some-json-editor"))
                .setSchemaEditor(new EditorConfiguration()
                        .setTitle("some-schema-editor"))

        instance == expected
    }

    def create(WindowProperties windowProperties, EditorConfiguration jsonConfig, EditorConfiguration schemaConfig) {
        return new Configuration()
                .setWindowProperties(windowProperties)
                .setJsonEditor(jsonConfig)
                .setSchemaEditor(schemaConfig)
    }
}
