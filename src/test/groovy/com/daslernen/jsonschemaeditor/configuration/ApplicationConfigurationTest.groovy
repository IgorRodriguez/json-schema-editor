package com.daslernen.jsonschemaeditor.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.testing.EqualsTester
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

class ApplicationConfigurationTest extends Specification {

    def mapper = new ObjectMapper()

    def "can create an instance"() {
        when:
        def underTest = new ApplicationConfiguration("some-url")

        then:
        underTest.getUpdateUrl() == "some-url"
    }

    def "implements equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new ApplicationConfiguration("abc"), new ApplicationConfiguration("abc"))
                .addEqualityGroup(new ApplicationConfiguration("123"), new ApplicationConfiguration("123"))
                .testEquals()

        then:
        noExceptionThrown()
    }

    def "serialises to expected JSON"() {
        given:
        def underTest = new ApplicationConfiguration("some-url")

        when:
        def json = mapper.writeValueAsString(underTest)

        then:
        def expected = """{ "updateUrl": "some-url" }"""
        JSONAssert.assertEquals(expected, json, true)
    }

    def "deserialises from JSON"() {
        given:
        def json = """{ "updateUrl": "some-url" }"""

        when:
        def configuration = mapper.readValue(json, ApplicationConfiguration.class)

        then:
        def expected = new ApplicationConfiguration("some-url")
        expected == configuration
    }
}
