package com.daslernen.jsonschemaeditor.configuration

import com.daslernen.jsonschemaeditor.domain.NewLineStyle
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.google.common.testing.EqualsTester
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

import java.nio.file.Paths

import static com.daslernen.jsonschemaeditor.domain.NewLineStyle.SYSTEM_DEFAULT_STYLE

class OpenFileTest extends Specification {

    def mapper = new ObjectMapper()
            .registerModule(new Jdk8Module())
            .configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

    def "properties are correctly assigned"() {
        when:
        def underTest = new OpenFile()
                                .setPath(Paths.get("/a/b"))
                                .setContent("some-content")
                                .setNewLineStyle(SYSTEM_DEFAULT_STYLE)
                                .setFontSize(2.2)

        then:
        underTest.getPath().get() == Paths.get("/a/b")
        underTest.getContent().get() == "some-content"
        underTest.getNewLineStyle() == SYSTEM_DEFAULT_STYLE
        underTest.getFontSize() == 2.2D
    }

    def "implements equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new OpenFile(), new OpenFile())
                .addEqualityGroup(create("/a/b", "content", SYSTEM_DEFAULT_STYLE, 1.1), create("/a/b", "content", SYSTEM_DEFAULT_STYLE, 1.1))
                .addEqualityGroup(create("/c/d", "content", SYSTEM_DEFAULT_STYLE, 1.1), create("/c/d", "content", SYSTEM_DEFAULT_STYLE, 1.1))
                .addEqualityGroup(create("/a/b", "other", SYSTEM_DEFAULT_STYLE, 1.1), create("/a/b", "other", SYSTEM_DEFAULT_STYLE, 1.1))
                .addEqualityGroup(create("/a/b", "content", SYSTEM_DEFAULT_STYLE, 2.2), create("/a/b", "content", SYSTEM_DEFAULT_STYLE, 2.2))
                .testEquals()
        then:
        noExceptionThrown()
    }

    def "can serialise to expected JSON"() {
        given:
        def expected = """
        {
            "path": "/a/b",
            "content": "some-content",
            "newLineStyle": {
                "value": "\n"
            },
            "fontSize": 2.2
        }
        """
        def underTest = create("/a/b", "some-content", NewLineStyle.findFromText("aa\n"), 2.2)

        when:
        def json = mapper.writeValueAsString(underTest)

        then:
        JSONAssert.assertEquals(expected, json, true)
    }

    def "can deserialise from JSON"() {
        given:
        def input = """
        {
            "path": "/a/b",
            "content": "some-content",
            "newLineStyle": {
                "value": "\n"
            },
            "fontSize": 2.2
        }
        """
        def expected = create("/a/b", "some-content", NewLineStyle.findFromText("aa\n"), 2.2)

        when:
        def deserialised = mapper.readValue(input, OpenFile)

        then:
        expected == deserialised
    }

    def create(String path, String content, NewLineStyle lineStyle, double fontSize) {
        return new OpenFile()
                .setPath(Paths.get(path))
                .setContent(content)
                .setNewLineStyle(lineStyle)
                .setFontSize(fontSize)
    }
}
