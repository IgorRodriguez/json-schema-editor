package com.daslernen.jsonschemaeditor.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.testing.EqualsTester
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

class WindowPropertiesTest extends Specification {

    def mapper = new ObjectMapper()

    def "implements equals and hashcode"() {
        when:
        new EqualsTester()
                .addEqualityGroup(new WindowProperties(), new WindowProperties())
                .addEqualityGroup(create(1.0, 2.0, 3.0, 4.0, 5.0), create(1.0, 2.0, 3.0, 4.0, 5.0))
                .addEqualityGroup(create(9.0, 2.0, 3.0, 4.0, 5.0), create(9.0, 2.0, 3.0, 4.0, 5.0))
                .addEqualityGroup(create(1.0, 9.0, 3.0, 4.0, 5.0), create(1.0, 9.0, 3.0, 4.0, 5.0))
                .addEqualityGroup(create(1.0, 2.0, 9.0, 4.0, 5.0), create(1.0, 2.0, 9.0, 4.0, 5.0))
                .addEqualityGroup(create(1.0, 2.0, 3.0, 9.0, 5.0), create(1.0, 2.0, 3.0, 9.0, 5.0))
                .addEqualityGroup(create(1.0, 2.0, 3.0, 4.0, 9.0), create(1.0, 2.0, 3.0, 4.0, 9.0))
                .testEquals()
        then:
        noExceptionThrown()
    }

    def "serialises to expected JSON"() {
        given:
        def expected = """
        {
            "x": 1.1,
            "y": 2.2,
            "width": 3.3,
            "height": 4.4,
            "scale": 5.5
        }
        """
        def underTest = create(1.1, 2.2, 3.3, 4.4, 5.5)

        when:
        def json = mapper.writeValueAsString(underTest)

        then:
        JSONAssert.assertEquals(expected, json, true)
    }

    def "deserialises from JSON"() {
        given:
        def expected = create(1.1, 2.2, 3.3, 4.4, 5.5)
        def input = """
        {
            "x": 1.1,
            "y": 2.2,
            "width": 3.3,
            "height": 4.4,
            "scale": 5.5
        }
        """
        when:
        def windowProperties = mapper.readValue(input, WindowProperties)

        then:
        expected == windowProperties
    }

    def create(double x, double y, double width, double height, double scale) {
        def properties = new WindowProperties()
        properties.setX(x)
        properties.setY(y)
        properties.setWidth(width)
        properties.setHeight(height)
        properties.setScale(scale)
        return properties
    }
}
