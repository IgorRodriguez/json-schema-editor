package com.daslernen.jsonschemaeditor.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.testing.EqualsTester
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

import java.nio.file.Paths

class EditorConfigurationTest extends Specification {

    private def objectMapper = new ObjectMapper()

    def "implements equals and hashcode"() {
        given:
        def openFile1 = Mock(OpenFile)
        def openFile2 = Mock(OpenFile)

        when:
        new EqualsTester()
                .addEqualityGroup(new EditorConfiguration(), new EditorConfiguration())
                .addEqualityGroup(create("aaaaa", "/bbbbb", openFile1, "ccccc"), create("aaaaa", "/bbbbb", openFile1, "ccccc"))
                .addEqualityGroup(create("other", "/bbbbb", openFile1, "ccccc"), create("other", "/bbbbb", openFile1, "ccccc"))
                .addEqualityGroup(create("aaaaa", "/other", openFile1, "ccccc"), create("aaaaa", "/other", openFile1, "ccccc"))
                .addEqualityGroup(create("aaaaa", "/bbbbb", openFile2, "ccccc"), create("aaaaa", "/bbbbb", openFile2, "ccccc"))
                .addEqualityGroup(create("aaaaa", "/bbbbb", openFile1, "other"), create("aaaaa", "/bbbbb", openFile1, "other"))
                .testEquals()

        then:
        noExceptionThrown()
    }

    def "can serialise to json"() {
        given:
        def instance = create("aa", "/bb", null, "cc")

        when:
        def json = objectMapper.writeValueAsString(instance)

        then:
        def expected = """{ "title": "aa", "lastOpenDirectory": "/bb", "openFile": null, "pathToRoot": "cc" }"""
        JSONAssert.assertEquals(expected, json, true)
    }

    def "can deserialise from json"() {
        given:
        def json = """{ 
                    "title": "aa", 
                    "lastOpenDirectory": "/bb", 
                    "openFile": null,
                    "pathToRoot": "cc"
                }"""

        when:
        def instance = objectMapper.readValue(json, EditorConfiguration)

        then:
        def expected = new EditorConfiguration()
                .setTitle("aa")
                .setLastOpenDirectory(Paths.get("/bb"))
                .setOpenFile(null)
                .setPathToRoot("cc")

        instance == expected
    }

    def create(String title, String lastOpenDirectory, OpenFile openFile, String pathToRoot) {
        return new EditorConfiguration()
                .setTitle(title)
                .setLastOpenDirectory(Paths.get(lastOpenDirectory))
                .setOpenFile(openFile)
                .setPathToRoot(pathToRoot)
    }
}
