# Json Schema Editor

[![build status](https://gitlab.com/IgorRodriguez/json-schema-editor/badges/master/build.svg)](https://gitlab.com/IgorRodriguez/json-schema-editor/commits/master)

A desktop application to edit and validate both JSON and JSON schema documents.

The main repository for this application is hosted in [https://gitlab.com/IgorRodriguez/json-schema-editor](https://gitlab.com/IgorRodriguez/json-schema-editor)

![Application preview](images/application_preview.png)

### Shortcuts

#### PC
 * `ctrl` + `alt` + `F` formats the JSON in the editor
 * `ctrl` + `F` shows the search dialog for the editor
 * `Esc` hides the search dialog when it has focus
 * `ctrl` + `mouse wheel` resizes the font of the editor

#### Mac
 * `⌘` + `⌥` + `F` formats the JSON in the editor
 * `⌘` + `F` shows the search dialog for the editor
 * `Esc` hides the search dialog when it has focus
 * `⌘` + `mouse wheel` resizes the font of the editor

## Requirements

 * JDK 8 version 45 or above

## Installation

Download the latest build from [here](https://gitlab.com/IgorRodriguez/json-schema-editor/builds/artifacts/master/download?job=package).

Unzip the file and run the *jar* file. To do so just double click over it (if this doesn't work you'll need to configure your system or execute `java -jar filename`)

